package body Double_Linked_List_Package is
    function Create return Double_Linked_List is
    begin
        return Double_Linked_List'(0, NULL, NULL, NULL);
    end Create;

    procedure Prepend_Element(List: in out Double_Linked_List; Element: in Element_Type) is
    begin
        -- If list is empty then create a link which is the current, the first and the last
        if List.Length = 0 then
            List.First := new Link'(Element, NULL, NULL);
            List.Last := List.First;
            List.Current := List.First;
        -- Else if list's current position is the first element then 
        --      create a new link (which will be the first) before the list's current
        elsif List.Current = List.First then
            List.Current.Previous := new Link'(Element, List.Current, List.Current.Previous);
            List.First := List.Current.Previous;
        -- Else create a link before the list's current 1
        else
            List.Current.Previous := new Link'(Element, List.Current, List.Current.Previous);
            List.Current.Previous.Previous.Next := List.Current.Previous;
        end if;
        -- Increase the length by 1
        List.Length := List.Length + 1;
    end Prepend_Element;
    
    procedure Append_Element(List: in out Double_Linked_List; Element: in Element_Type) is
    begin
        -- If list is empty then create a link which is the current, the first and the last
        if List.Length = 0 then
            List.Current := new Link'(Element, NULL, NULL);
            List.First := List.Current;
            List.Last := List.Current;
        -- Else if list's current position is the first element then 
        --      create a new link (which will be the first) before the list's current
        elsif List.Current = List.Last then
            List.Current.Next := new Link'(Element, List.Current.Next, List.Current);
            List.Last := List.Current.Next;
        -- Else create a link after the list's current element.
        else
            List.Current.Next := new Link'(Element, List.Current.Next, List.Current);
            List.Current.Next.Next.Previous := List.Current.Next;
        end if;
        -- Increase the length by 1
        List.Length := List.Length + 1;
    end Append_Element;

    procedure Prepend_First_Element(List: in out Double_Linked_List; Element: in Element_Type) is
        New_Link: Ptr_Link;
    begin
        -- If list is empty then create a link which is the current, the first and the last
        if List.Length = 0 then
            List.Current := new Link'(Element, NULL, NULL);
            List.First := List.Current;
            List.Last := List.Current;
        -- Else create a new link (which will be the first)
        else
            New_Link := new Link'(Element, List.First, NULL);
            List.First.Previous := New_Link;
            List.First := New_Link;
        end if;
        -- Increase the length by 1
        List.Length := List.Length + 1;
    end Prepend_First_Element;

    procedure Append_Last_Element(List: in out Double_Linked_List; Element: in Element_Type) is
        New_Link: Ptr_Link;
    begin
        -- If list is empty then create a link which is the current, the first and the last
        if List.Length = 0 then
            List.Current := new Link'(Element, NULL, NULL);
            List.First := List.Current;
            List.Last := List.Current;
        -- Else create a new link (which will be the last)
        else
            New_Link := new Link'(Element, NULL, List.Last);
            List.Last.Next := New_Link;
            List.Last := New_Link;
        end if;
        -- Increase the length by 1
        List.Length := List.Length + 1;
    end Append_Last_Element;

    procedure Delete_Element(List: in out Double_Linked_List) is
    begin
        -- If list is empty then error
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        -- Else if list's length is equals to 1 then
        elsif List.Length = 1 then
            List.First := NULL;
            List.Last := NULL;
            List.Current := NULL;
        -- Else if list's current is the first element then list's current and list's first become next element
        elsif List.Current = List.First then
            List.First := List.Current.Next;
            List.First.Previous := NULL;
            List.Current := List.First;
        -- Else if list's current is the last element then list's current and list's last become previous element
        elsif List.Current = List.Last then
            List.Last := List.Current.Previous;
            List.Last.Next := NULL;
            List.Current := List.Last;
        -- Else rearrange link between list's previous and next
        else
            List.Current.Previous.Next := List.Current.Next;
            List.Current.Next.Previous := List.Current.Previous;
            List.Current := List.Current.Previous;
        end if;
        -- Decrease length by 1
        List.Length := List.Length - 1;
    end Delete_Element;

    function Get_Length(List: in Double_Linked_List) return Natural is
    begin
        return List.Length;
    end Get_Length;

    function Get_First_Element(List: in Double_Linked_List) return Element_Type is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        else
            return List.First.Element;
        end if;
    end Get_First_Element;

    function Get_Last_Element(List: in Double_Linked_List) return Element_Type is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        else
            return List.Last.Element;
        end if;
    end Get_Last_Element;

    function Get_Element(List: in Double_Linked_List) return Element_Type is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        else
            return List.Current.Element;
        end if;
    end Get_Element;

    function Has_Next(List: in out Double_Linked_List) return Boolean is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        else
            return List.Current.Next /= NULL;
        end if;
    end Has_Next;

    function Has_Previous(List: in out Double_Linked_List) return Boolean is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        else
            return List.Current.Previous /= NULL;
        end if;
    end Has_Previous;

    function Get_Next_Element(List: in out Double_Linked_List) return Element_Type is
    begin
        if Has_Next(List) then
            return List.Current.Next.Element;
        else
            raise EXCEPTION_LIST_NO_MORE_ELEMENT;
        end if;
    end Get_Next_Element;

    function Get_Previous_Element(List: in out Double_Linked_List) return Element_Type is
    begin
        if Has_Previous(List) then
            return List.Current.Previous.Element;
        else
            raise EXCEPTION_LIST_NO_MORE_ELEMENT;
        end if;
    end Get_Previous_Element;

    procedure Next(List: in out Double_Linked_List) is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        elsif List.Current.Next /= NULL then
            List.Current := List.Current.Next;
        else
            raise EXCEPTION_LIST_NO_MORE_ELEMENT;
        end if;
    end Next;

    procedure Previous(List: in out Double_Linked_List) is
    begin
        if List.Length = 0 then
            raise EXCEPTION_LIST_EMPTY;
        elsif List.Current.Previous /= NULL then
            List.Current := List.Current.Previous;
        else
            raise EXCEPTION_LIST_NO_MORE_ELEMENT;
        end if;
    end Previous;

    procedure Clear(List: in out Double_Linked_List) is
    begin
        List.Length := 0;
        List.First := NULL;
        List.Current := NULL;
        List.Last := NULL;
    end Clear;

    procedure Go_To(List: in out Double_Linked_List; Element: in Element_Type) is
        Real_Current_Link: Ptr_Link;
    begin
        -- If list is empty then raise error
        if List.Length = 0 then
            raise EXCEPTION_LIST_ELEMENT_NOT_FOUND;
        -- else if element to go is the list's first then list's current element become list's first element
        elsif List.First.Element = Element then
            List.Current := List.First;
        -- else if element to go is the list's last then list's current element become list's last element
        elsif List.Last.Element = Element then
            List.Current := List.Last;
        -- else go to the first element
        -- while list's current element is not the last element and list's current element is not the element to go loop
        --      iterate throught the list
        -- end loop
        -- if list's current element is not the element to go to then come back to the real current
        else
            Real_Current_Link := List.Current;
            List.Current := List.First;
            while List.Current /= List.Last and List.Current.Element /= Element loop
                List.Current := List.Current.Next;
            end loop;

            if List.Current.Element /= Element then
                List.Current := Real_Current_Link;
                raise EXCEPTION_LIST_ELEMENT_NOT_FOUND;
            end if;
        end if;
    end Go_To;
end Double_Linked_List_Package;
