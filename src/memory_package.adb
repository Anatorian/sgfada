package body Memory_Package is
    function Create(Start_Index: in Natural; Total_Size: in Natural) return Memory_Allocator is
        Memory_Blocks_Free: Double_Linked_List := Create;
    begin
        -- add to free spaces a bloc with total size parameter and return allocator
        Append_Element(Memory_Blocks_Free, Create(Start_Index, Total_Size));
        return Memory_Allocator'(Start_Index, Total_Size, Memory_Blocks_Free, Create);
    end Create;


    function Allocate_Memory(Allocator: in out Memory_Allocator; Physical_Size: in Natural) return Ptr_Memory_Block is
        Current: Ptr_Memory_Block;
        Memory_Block_Return: Ptr_Memory_Block;
    begin
        -- is enough memory block then
        if Get_Length(Allocator.Memory_Blocks_Free) > 0 then
            -- iterate on free spaces and when a block is with a size > physicial size      
            Go_To(Allocator.Memory_Blocks_Free, Get_First_Element(Allocator.Memory_Blocks_Free));
            loop
                Current := Get_Element(Allocator.Memory_Blocks_Free);
                exit when not Has_Next(Allocator.Memory_Blocks_Free) or Get_Physical_Size(Current) >= Physical_Size;
                Next(Allocator.Memory_Blocks_Free);
            end loop;

            -- if block size = physical size then return it
            -- else if block size > physical size then rearrange start index of the free space, create a block with the physical size and return it
            -- else error
            if Get_Physical_Size(Current) = Physical_Size then
                Memory_Block_Return := Current;
                Delete_Element(Allocator.Memory_Blocks_Free);
            elsif Get_Physical_Size(Current) > Physical_Size then
                Memory_Block_Return := Create(Get_Start_Index(Current), Physical_Size);
                Set_Start_Index(Current, Get_Start_Index(Current) + Physical_Size);
                Set_Physical_Size(Current, Get_Physical_Size(Current) - Physical_Size);
            else
                raise EXCEPTION_NOT_ENOUGH_MEMORY;
            end if;
            Append_Last_Element(Allocator.Memory_Blocks_Used, Memory_Block_Return);
            return Memory_Block_Return;
        -- else error
        else
            raise EXCEPTION_NOT_ENOUGH_MEMORY;
        end if;
    end Allocate_Memory;


    procedure Deallocate_Memory(Allocator: in out Memory_Allocator; Memory_Block: in Ptr_Memory_Block) is
    begin
        -- delete memory block from memory block used list
        Go_To(Allocator.Memory_Blocks_Used, Memory_Block);
        Delete_Element(Allocator.Memory_Blocks_Used);
        if Get_Length(Allocator.Memory_Blocks_Free) = 0 then
            Append_Element(Allocator.Memory_Blocks_Free, Memory_Block);
        else
            -- iterate through free space and insert memory block by start index in increasing order
            loop
                exit when not Has_Next(Allocator.Memory_Blocks_Free) or Get_Start_Index(Memory_Block) + Get_Physical_Size(Memory_Block) <= Get_Start_Index(Get_Element(Allocator.Memory_Blocks_Free));
                Next(Allocator.Memory_Blocks_Free);
            end loop;
            
            if Get_Start_Index(Memory_Block) + Get_Physical_Size(Memory_Block) <= Get_Start_Index(Get_Element(Allocator.Memory_Blocks_Free)) then
                Prepend_Element(Allocator.Memory_Blocks_Free, Memory_Block);
            else
                Append_Element(Allocator.Memory_Blocks_Free, Memory_Block);
            end if;
        end if;
        -- when added merge all memory block if possible
        Merge_Memory(Allocator);
    exception
        when EXCEPTION_LIST_ELEMENT_NOT_FOUND => raise EXCEPTION_MEMORY_BLOCK_NOT_FOUND;
    end Deallocate_Memory;


    procedure Rearrange_Memory(Allocator: in out Memory_Allocator) is
        Start_Index: Natural := Allocator.Start_Index;
        Total_Size_Used: Natural := 0;
        Current_Memory_Block: Ptr_Memory_Block;
    begin
        -- iterate through all memory block in memory block used list and change all start index to compact
        Go_To(Allocator.Memory_Blocks_Used, Get_First_Element(Allocator.Memory_Blocks_Used));
        loop
            Current_Memory_Block := Get_Element(Allocator.Memory_Blocks_Used);
            Set_Start_Index(Current_Memory_Block, Start_Index);
            Start_Index := Get_End_Index(Current_Memory_Block) + 1;
            Total_Size_Used := Get_Physical_Size(Current_Memory_Block);
            exit when not Has_Next(Allocator.Memory_Blocks_Used);
            Next(Allocator.Memory_Blocks_Used);
        end loop;
        -- create a free space from last memory block used to total size possible
        Clear(Allocator.Memory_Blocks_Free);
        Append_Element(Allocator.Memory_Blocks_Free, Create(Start_Index, Allocator.Total_Size - Total_Size_Used));
    end Rearrange_Memory;


    procedure Merge_Memory(Allocator: in out Memory_Allocator) is
        Previous: Ptr_Memory_Block;
        Current: Ptr_Memory_Block;
    begin
        -- iterate through all memory block free and if start index + physical size = next memory block start index then merge into one memory block
        if Get_Length(Allocator.Memory_Blocks_Free) > 1 then
            Go_To(Allocator.Memory_Blocks_Free, Get_First_Element(Allocator.Memory_Blocks_Free));
            Next(Allocator.Memory_Blocks_Free);
            loop
                Previous := Get_Previous_Element(Allocator.Memory_Blocks_Free);
                Current := Get_Element(Allocator.Memory_Blocks_Free);
                if Get_Start_Index(Previous) + Get_Physical_Size(Previous) = Get_Start_Index(Current) then
                    Set_Physical_Size(Previous, Get_Physical_Size(Previous) + Get_Physical_Size(Current));
                    Delete_Element(Allocator.Memory_Blocks_Free);
                end if;
                exit when not Has_Next(Allocator.Memory_Blocks_Free);
                Next(Allocator.Memory_Blocks_Free);
            end loop;
        end if;
    end Merge_Memory;
end Memory_Package;
