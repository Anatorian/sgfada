with Inode_Package; use Inode_Package;
with Memory_Block_Package; use Memory_Block_Package;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Calendar; use Ada.Calendar;

procedure Test_Inode is
    EXCEPTION_NOT_OK: Exception;

    Inode: Ptr_Inode;
begin
    begin
        Inode := Create(Folder, Create(10, 5));
        Inode := Create(Ordinary_File, Create(10, 5));
        Put_Line("Create OK");
        exception
            when others => Put_Line("Create NOK");
    end;

    begin
        Inode := NULL;
        begin
            if Get_Inode_Type(Inode) = Ordinary_File then
                NULL;
            end if;
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_INODE_NULL => NULL;
        end;
        Inode := Create(Ordinary_File,  Create(10, 5));
        if Get_Inode_Type(Inode) /= Ordinary_File then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Get_Inode_Type OK");
        exception
            when others => Put_Line("Get_Inode_Type NOK");
    end;

    begin
        Inode := NULL;
        begin
            if Get_Creation_Date_Time(Inode) = Clock then
                NULL;
            end if;
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_INODE_NULL => NULL;
        end;
        Put_Line("Create OK");
        exception
            when others => Put_Line("Get_Creation_Date_Time NOK");
    end;

    begin
        Inode := NULL;
        begin
            if Get_Modification_Date_Time(Inode) = Clock then
                NULL;
            end if;
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_INODE_NULL => NULL;
        end;
        Put_Line("Get_Modification_Date_Time OK");
        exception
            when others => Put_Line("Get_Modification_Date_Time NOK");
    end;

    begin
        Inode := NULL;
        begin
            Set_Modification_Date_Time(Inode, Clock);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_INODE_NULL => NULL;
        end;
        Put_Line("Set_Modification_Date_Time OK");
        exception
            when others => Put_Line("Set_Modification_Date_Time NOK");
    end;
end Test_Inode;
