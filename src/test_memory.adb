with Memory_Package; use Memory_Package;
with Memory_Block_Package; use Memory_Block_Package;
with Ada.Text_IO; use Ada.Text_IO;

procedure Test_Memory is
    EXCEPTION_NOT_OK: Exception;

    Memory_Block1: Ptr_Memory_Block;
    Memory_Block2: Ptr_Memory_Block;
    Memory_Alloc: Memory_Allocator;
begin
    begin
        Memory_Alloc := Create(Start_Index => 1, Total_Size => 100);
        Put_Line("Create OK");
    exception
        when others => Put_Line("Create NOK");
    end;

    begin
        begin
            Memory_Block1 := Allocate_Memory(Memory_Alloc, 200);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_NOT_ENOUGH_MEMORY => NULL;
        end;
        Memory_Block1 := Allocate_Memory(Memory_Alloc, 10);
        if Get_Start_Index(Memory_Block1) /= 1 or Get_Physical_Size(Memory_Block1) /= 10 then
            raise EXCEPTION_NOT_OK;
        end if;
        Memory_Block2 := Allocate_Memory(Memory_Alloc, 10);
        if Get_Start_Index(Memory_Block2) /= 11 or Get_Physical_Size(Memory_Block2) /= 10 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Allocate_Memory OK");
    exception
        when others => Put_Line("Allocate_Memory NOK");
    end;

    begin
        Deallocate_Memory(Memory_Alloc, Memory_Block1);
        Memory_Block1 := Allocate_Memory(Memory_Alloc, 10);
        if Get_Start_Index(Memory_Block1) /= 1 and Get_Physical_Size(Memory_Block1) /= 10 then
            raise EXCEPTION_NOT_OK;
        end if;
        Deallocate_Memory(Memory_Alloc, Memory_Block2);
        Deallocate_Memory(Memory_Alloc, Memory_Block1);
        Memory_Block1 := Allocate_Memory(Memory_Alloc, 10);
        if Get_Start_Index(Memory_Block1) /= 1 and Get_Physical_Size(Memory_Block1) /= 10 then
            raise EXCEPTION_NOT_OK;
        end if;
        Deallocate_Memory(Memory_Alloc, Memory_Block1);
        Put_Line("Deallocate_Memory and Merge_Memory OK");
    exception
        when others => Put_Line("Deallocate_Memory and Merge_Memory NOK");
    end;

    begin
        Memory_Block1 := Allocate_Memory(Memory_Alloc, 10);
        Memory_Block2 := Allocate_Memory(Memory_Alloc, 10);
        Deallocate_Memory(Memory_Alloc, Memory_Block1);
        Rearrange_Memory(Memory_Alloc);
        if Get_Start_Index(Memory_Block1) /= 1 and Get_Physical_Size(Memory_Block1) /= 10 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Rearrange_Memory OK");
    exception
        when others => Put_Line("Rearrange_Memory NOK");
    end;
end Test_Memory;
