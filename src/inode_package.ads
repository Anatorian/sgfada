with Ada.Calendar; use Ada.Calendar;
with Memory_Block_Package; use Memory_Block_Package;

package Inode_Package is
    type Inode is private;
    type Ptr_Inode is access Inode;
    type Inode_Type is (Folder, Ordinary_File);

    EXCEPTION_INODE_NULL: Exception;

    -- Name: Create
    -- Semantics: Create a new Ptr_Inode
    -- Parameters:
    --      I_Type: Inode_Type of the inode.
    --      Memory_Block: Memory_Block to assign to Inode.
    -- Return type: Ptr_Inode
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Create(I_Type: in Inode_Type; Memory_Block: in Ptr_Memory_Block) return Ptr_Inode;

    -- Name: Get_Inode_Type
    -- Semantics: Return the inode's type of the Ptr_Inode sent as parameter. 
    -- Parameters:
    --      Inode: Ptr_Inode from which we want the Inode_Type.
    -- Return type: Inode_Type
    -- Exceptions: EXCEPTION_INODE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Inode_Type(Inode: in Ptr_Inode) return Inode_Type;

    -- Name: Get_Memory_Block
    -- Semantics: Return the memory block of the Ptr_Inode sent as parameter. 
    -- Parameters:
    --      Inode: Ptr_Inode from which we want the Ptr_Memory_Block.
    -- Return type: Ptr_Memory_Block
    -- Exceptions: EXCEPTION_INODE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Memory_Block(Inode: in Ptr_Inode) return Ptr_Memory_Block;

    -- Name: Get_Creation_Date_Time
    -- Semantics: Return the creation date time of the Ptr_Inode sent as parameter. 
    -- Parameters:
    --      Inode: Ptr_Inode from which we want the Time (Creation date time).
    -- Return type: Ada.Calendar.Time
    -- Exceptions: EXCEPTION_INODE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Creation_Date_Time(Inode: in Ptr_Inode) return Time;

    -- Name: Get_Modification_Date_Time
    -- Semantics: Return the modification date time of the Ptr_Inode sent as parameter. 
    -- Parameters:
    --      Inode: Ptr_Inode from which we want the Time (Modification date time).
    -- Return type: Ada.Calendar.Time
    -- Exceptions: EXCEPTION_INODE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Modification_Date_Time(Inode: in Ptr_Inode) return Time;

    -- Name: Set_Memory_Block
    -- Semantics: Change the Ptr_Inode's memory block by the memory block sent as parameter.
    -- Parameters:
    --      Inode: The Ptr_Inode to change 
    --      Memory_Block: New inode memory block
    -- Exceptions: EXCEPTION_INODE_NULL
    -- Preconditions: ~
    -- Postconditions: Inode.Memory_Block = Memory_Block
    procedure Set_Memory_Block(Inode: in Ptr_Inode; Memory_Block: in Ptr_Memory_Block);

    -- Name: Set_Modification_Date_Time
    -- Semantics: Change the Ptr_Inode's memory block by the time sent as parameter.
    -- Parameters:
    --      Inode: The Ptr_Inode to change 
    --      Modification_Date_Time: New inode modification date time
    -- Exceptions: EXCEPTION_INODE_NULL
    -- Preconditions: ~
    -- Postconditions: Inode.Modification_Date_Time = Modification_Date_Time
    procedure Set_Modification_Date_Time(Inode: in Ptr_Inode; Modification_Date_Time: in Time);
private
    type Inode is record
        I_Type: Inode_Type;
        Memory_Block: Ptr_Memory_Block;
        Creation_Date_Time: Time;
        Modification_Date_Time: Time;
    end record;
end Inode_Package;
