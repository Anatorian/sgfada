package Memory_Block_Package is
    type Memory_Block is private;
    type Ptr_Memory_Block is access Memory_Block;

    EXCEPTION_MEMORY_BLOCK_NULL: Exception;

    -- Name: Create
    -- Semantics: Create a new Ptr_Memory_Block
    -- Parameters:
    --      Start_Index: Index from which the memory block starts
    --      Physical_Size: Size of the memory block
    -- Return type: Ptr_Memory_Block
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Create(Start_Index: in Natural; Physical_Size: in Natural) return Ptr_Memory_Block;

    -- Name: Get_Start_Index
    -- Semantics: Return the start index of the Ptr_Memory_Block sent as parameter.
    -- Parameters:
    --      Memory_Block: The Ptr_Memory_Block from which we want the start index. 
    -- Return type: Natural
    -- Exceptions: EXCEPTION_MEMORY_BLOCK_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Start_Index(Memory_Block: in Ptr_Memory_Block) return Natural;

    -- Name: Get_End_Index
    -- Semantics: Return the end index of the Ptr_Memory_Block sent as parameter.
    -- Parameters:
    --      Memory_Block: The Ptr_Memory_Block from which we want the end index. 
    -- Return type: Natural
    -- Exceptions: EXCEPTION_MEMORY_BLOCK_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_End_Index(Memory_Block: in Ptr_Memory_Block) return Natural;

    -- Name: Get_Physical_Size
    -- Semantics: Return the physical size of the Ptr_Memory_Block sent as parameter.
    -- Parameters:
    --      Memory_Block: The Ptr_Memory_Block from which we want the physical size. 
    -- Return type: Natural
    -- Exceptions: EXCEPTION_MEMORY_BLOCK_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Physical_Size(Memory_Block: in Ptr_Memory_Block) return Natural;

    -- Name: Set_Start_Index
    -- Semantics: Change the Ptr_Memory_Block's start index by the start index sent as parameter.
    -- Parameters:
    --      Memory_Block: The Ptr_Memory_Block to change
    --      Start_Index: New memory block start index
    -- Exceptions: EXCEPTION_MEMORY_BLOCK_NULL
    -- Preconditions: ~
    -- Postconditions: Memory_Block.Start_Index = Start_Index
    procedure Set_Start_Index(Memory_Block: in Ptr_Memory_Block; Start_Index: in Natural);

    -- Name: Create
    -- Semantics: Change the Ptr_Memory_Block's physical size by the physical size sent as parameter.
    -- Parameters:
    --      Memory_Block: The Ptr_Memory_Block to change 
    --      Physical_Size: New memory block physical size
    -- Exceptions: EXCEPTION_MEMORY_BLOCK_NULL
    -- Preconditions: ~
    -- Postconditions: Memory_Block.Physical_Size = Physical_Size
    procedure Set_Physical_Size(Memory_Block: in Ptr_Memory_Block; Physical_Size: in Natural);
private
    type Memory_Block is record
        Start_Index: Natural;
        Physical_Size: Natural;
    end record;
end Memory_Block_Package;
