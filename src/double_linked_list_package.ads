generic
    type Element_Type is private;
    with function "="(Element1: in Element_Type; Element2: in Element_Type) return Boolean;
package Double_Linked_List_Package is
    type Double_Linked_List is private;

    EXCEPTION_LIST_EMPTY: Exception;
    EXCEPTION_LIST_ELEMENT_NOT_FOUND: Exception;
    EXCEPTION_LIST_NO_MORE_ELEMENT: Exception;

    -- Name: Create
    -- Semantics: Create a new Double_Linked_List
    -- Parameters: ~
    -- Return type: Double_Linked_List
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Create return Double_Linked_List;
    
    -- Name: Prepend_Element
    -- Semantics: Prepend an element before current's position in the list.
    -- Parameters:
    --      List: Double_Linked_List from which we want to prepend an element.
    --      Element: The Element_Type to prepend in the list.
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: List.Length = List.Length'Old + 1
    procedure Prepend_Element(List: in out Double_Linked_List; Element: in Element_Type);

    -- Name: Append_Element
    -- Semantics: Append an element after current's position in the list.
    -- Parameters:
    --      List: Double_Linked_List from which we want to prepend an element.
    --      Element: The Element_Type to append in the list.
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: List.Length = List.Length'Old + 1
    procedure Append_Element(List: in out Double_Linked_List; Element: in Element_Type);

    -- Name: Prepend_First_Element
    -- Semantics: Prepend an element to the list's first position.
    -- Parameters:
    --      List: Double_Linked_List from which we want to prepend an element.
    --      Element: The Element_Type to prepend first in the list.
    -- Return type: Inode_Type
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: List.Length = List.Length'Old + 1
    procedure Prepend_First_Element(List: in out Double_Linked_List; Element: in Element_Type);

    -- Name: Append_Last_Element
    -- Semantics: Append an element to the list's last position.
    -- Parameters:
    --      List: Double_Linked_List from which we want to append an element.
    --      Element: The Element_Type to prepend in the list.
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: List.Length = List.Length'Old + 1
    procedure Append_Last_Element(List: in out Double_Linked_List; Element: in Element_Type);

    -- Name: Prepend_Element
    -- Semantics: Delete the element at the list's current position.
    -- Parameters:
    --      List: Double_Linked_List from which we want to delete an element.
    -- Exceptions: EXCEPTION_LIST_EMPTY
    -- Preconditions: ~
    -- Postconditions: List.Length = List.Length'Old - 1
    procedure Delete_Element(List: in out Double_Linked_List);

    -- Name: Get_Length
    -- Semantics: Return length of the Double_Linked_List sent as parameter. 
    -- Parameters:
    --      List: Double_Linked_List from which we want the length.
    -- Return type: Natural
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Length(List: in Double_Linked_List) return Natural;

    -- Name: Get_First_Element
    -- Semantics: Return the first element of the Double_Linked_List sent as parameter. 
    -- Parameters:
    --      List: Double_Linked_List from which we want the first element.
    -- Return type: Element_Type
    -- Exceptions: EXCEPTION_LIST_EMPTY
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_First_Element(List: in Double_Linked_List) return Element_Type;

    -- Name: Get_Last_Element
    -- Semantics: Return the last element of the Double_Linked_List sent as parameter. 
    -- Parameters:
    --      List: Double_Linked_List from which we want the last element.
    -- Return type: Element_Type
    -- Exceptions: EXCEPTION_LIST_EMPTY
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Last_Element(List: in Double_Linked_List) return Element_Type;

    -- Name: Get_Element
    -- Semantics: Return the element at the Double_Linked_List's current position sent as parameter. 
    -- Parameters:
    --      List: Double_Linked_List from which we want the element at the current position.
    -- Return type: Element_Type
    -- Exceptions: EXCEPTION_LIST_EMPTY
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Element(List: in Double_Linked_List) return Element_Type;

    -- Name: Has_Next
    -- Semantics: Return a boolean if Double_Linked_List is not empty in order to advertise if a next element exists. 
    -- Parameters:
    --      List: Double_Linked_List from which we want to know if a next element exist.
    -- Return type: Boolean
    -- Exceptions: EXCEPTION_LIST_EMPTY
    -- Preconditions: ~
    -- Postconditions: True if List.Current.Next /= NULL else False
    function Has_Next(List: in out Double_Linked_List) return Boolean;

    -- Name: Has_Next
    -- Semantics: Return a boolean if Double_Linked_List is not empty in order to advertise if a previous element exists. 
    -- Parameters:
    --      List: Double_Linked_List from which we want to know if a previous element exist.
    -- Return type: Boolean
    -- Exceptions: EXCEPTION_LIST_EMPTY
    -- Preconditions: ~
    -- Postconditions: True if List.Current.Previous /= NULL else False
    function Has_Previous(List: in out Double_Linked_List) return Boolean;

    -- Name: Get_Next_Element
    -- Semantics: Return Double_Linked_List's next element if the list is not empty and if a next element exists. 
    -- Parameters:
    --      List: Double_Linked_List from which we want the next element.
    -- Return type: Element_Type
    -- Exceptions: EXCEPTION_LIST_EMPTY, EXCEPTION_LIST_NO_MORE_ELEMENT
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Next_Element(List: in out Double_Linked_List) return Element_Type;

    -- Name: Get_Previous_Element
    -- Semantics: Return Double_Linked_List's previous element if the list is not empty and if a previous element exists. 
    -- Parameters:
    --      List: Double_Linked_List from which we want the next element.
    -- Return type: Element_Type
    -- Exceptions: EXCEPTION_LIST_EMPTY, EXCEPTION_LIST_NO_MORE_ELEMENT
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Previous_Element(List: in out Double_Linked_List) return Element_Type;

    -- Name: Next
    -- Semantics: Iterate throught the Double_Linked_List if a next element exists.
    -- Parameters:
    --      List: Double_Linked_List from which we want to iterate.
    -- Exceptions: EXCEPTION_LIST_EMPTY, EXCEPTION_LIST_NO_MORE_ELEMENT
    -- Preconditions: ~
    -- Postconditions: List.Current'New = List.Current'Old.Next
    procedure Next(List: in out Double_Linked_List);

    -- Name: Previous
    -- Semantics: Iterate throught the Double_Linked_List if a previous element exists.
    -- Parameters:
    --      List: Double_Linked_List from which we want to iterate.
    -- Exceptions: EXCEPTION_LIST_EMPTY, EXCEPTION_LIST_NO_MORE_ELEMENT
    -- Preconditions: ~
    -- Postconditions: List.Current'New = List.Current'Old.Previous
    procedure Previous(List: in out Double_Linked_List);

    -- Name: Clear
    -- Semantics: Clear the entire Double_Linked_List sent as parameter.
    -- Parameters:
    --      List: Double_Linked_List we want to clear.
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: List.Length = 0
    procedure Clear(List: in out Double_Linked_List);

    -- Name: Go_To
    -- Semantics: Place the Double_Linked_List's current position to the element if it exists.
    -- Parameters:
    --      List: Double_Linked_List from which we want to place the current position to the element.
    --      Element: Element_Type for the new Double_Linked_List's current position.
    -- Exceptions: EXCEPTION_LIST_EMPTY, EXCEPTION_LIST_ELEMENT_NOT_FOUND
    -- Preconditions: ~
    -- Postconditions: List.Current is at element position
    procedure Go_To(List: in out Double_Linked_List; Element: in Element_Type);
private
    type Link;
    type Ptr_Link is access Link;
    type Link is record
        Element: Element_Type;
        Next: Ptr_Link;
        Previous: Ptr_Link;
    end record;

    type Double_Linked_List is record
        Length: Natural;
        First: Ptr_Link;
        Last: Ptr_Link;
        Current: Ptr_Link;
    end record;
end Double_Linked_List_Package;
