package body Inode_Package is
    function Create(I_Type: in Inode_Type; Memory_Block: in Ptr_Memory_Block) return Ptr_Inode is
    begin
        return new Inode'(I_Type, Memory_Block, Clock, Clock);
    end Create;

    function Get_Inode_Type(Inode: in Ptr_Inode) return Inode_Type is
    begin
        if Inode = NULL then
            raise EXCEPTION_INODE_NULL;
        else
            return Inode.I_Type;
        end if;
    end Get_Inode_Type;


    function Get_Memory_Block(Inode: in Ptr_Inode) return Ptr_Memory_Block is
    begin
        if Inode = NULL then
            raise EXCEPTION_INODE_NULL;
        else
            return Inode.Memory_Block;
        end if;
    end Get_Memory_Block;

    function Get_Creation_Date_Time(Inode: in Ptr_Inode) return Time is
    begin
        if Inode = NULL then
            raise EXCEPTION_INODE_NULL;
        else
            return Inode.Creation_Date_Time;
        end if;
    end Get_Creation_Date_Time;

    function Get_Modification_Date_Time(Inode: in Ptr_Inode) return Time is
    begin
        if Inode = NULL then
            raise EXCEPTION_INODE_NULL;
        else
            return Inode.Modification_Date_Time;
        end if;
    end Get_Modification_Date_Time;

    procedure Set_Memory_Block(Inode: in Ptr_Inode; Memory_Block: in Ptr_Memory_Block) is
    begin
        if Inode = NULL then
            raise EXCEPTION_INODE_NULL;
        else
            Inode.Memory_Block := Memory_Block;
        end if;
    end Set_Memory_Block;

    procedure Set_Modification_Date_Time(Inode: in Ptr_Inode; Modification_Date_Time: in time) is
    begin
        if Inode = NULL then
            raise EXCEPTION_INODE_NULL;
        else
            Inode.Modification_Date_Time := Modification_Date_Time;
        end if;
    end Set_Modification_Date_Time;
end Inode_Package;
