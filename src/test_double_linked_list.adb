with Double_Linked_List_Package;
with Ada.Text_IO; use Ada.Text_IO;

procedure Test_Double_Linked_List is
    EXCEPTION_NOT_OK: exception;

    package List_Package is new Double_Linked_List_Package(Element_Type => Integer, "=" => "=");
    use List_Package;

    List: Double_Linked_List;
    Trash: Integer;
begin
    begin
        List := Create;
        Prepend_Element(List, 1);
        Prepend_Element(List, 2);
        if Get_Element(List) /= 1 or else Get_Previous_Element(List) /= 2 or else Get_Length(List) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Prepend_Element OK");
        exception
            when others => Put_Line("Prepend_Element NOK");
    end;

    begin
        List := Create;
        Append_Element(List, 1);
        Append_Element(List, 2);
        if Get_Element(List) /= 1 or else Get_Next_Element(List) /= 2 or else Get_Length(List) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Append_Element OK");
        exception
            when others => Put_Line("Append_Element NOK");
    end;

    begin
        List := Create;
        Prepend_First_Element(List, 1);
        Prepend_First_Element(List, 2);
        if Get_Element(List) /= 1 or else Get_First_Element(List) /= 2 or else Get_Length(List) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Prepend_First_Element OK");
        exception
            when others => Put_Line("Prepend_First_Element NOK");
    end;
    
    begin
        List := Create;
        Append_Last_Element(List, 1);
        Append_Last_Element(List, 2);
        if Get_Element(List) /= 1 or else Get_Last_Element(List) /= 2 or else Get_Length(List) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        exception
            when others => Put_Line("Append_Last_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            Delete_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Element(List, 2);
        Delete_Element(List);
        if Get_Length(List) /= 0 then
            raise EXCEPTION_NOT_OK;
        end if;
        Append_Element(List, 1);
        Append_Element(List, 2);
        Append_Element(List, 3);
        Go_To(List, Get_Last_Element(List));
        Delete_Element(List);
        Go_To(List, Get_First_Element(List));
        Delete_Element(List);
        Delete_Element(List);
        Put_Line("Delete_Element OK");
        exception
            when others => Put_Line("Delete_Element NOK");
    end;
    
    begin
        List := Create;
        if Get_Length(List) /= 0 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Get_Length OK");
        exception
            when others => Put_Line("Get_Length NOK");
    end;

    begin
        List := Create;
        begin
            Trash := Get_First_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Prepend_First_Element(List, 1);
        if Get_First_Element(List) /= 1 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Get_First_Element OK");
        exception
            when others => Put_Line("Get_First_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            Trash := Get_Last_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Last_Element(List, 1);
        if Get_Last_Element(List) /= 1 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Get_Last_Element OK");
        exception
            when others => Put_Line("Get_Last_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            Trash := Get_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Last_Element(List, 1);
        if Get_Element(List) /= 1 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Get_Element OK");
        exception
            when others => Put_Line("Get_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            Trash := Get_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Last_Element(List, 1);
        if Get_Element(List) /= 1 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Get_Element OK");
        exception
            when others => Put_Line("Get_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            if Has_Next(List) then
                raise EXCEPTION_NOT_OK;
            end if;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Last_Element(List, 1);
        if Has_Next(List) then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Has_Next OK");
        exception
            when others => Put_Line("Has_Next NOK");
    end;

    begin
        List := Create;
        begin
            Trash := Get_Next_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Last_Element(List, 1);
        begin
            Trash := Get_Next_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_NO_MORE_ELEMENT => null;
        end;
        Append_Last_Element(List, 2);
        Trash := Get_Next_Element(List);
        Put_Line("Get_Next_Element OK");
        exception
            when others => Put_Line("Get_Next_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            Trash := Get_Previous_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Prepend_First_Element(List, 1);
        begin
            Trash := Get_Previous_Element(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_NO_MORE_ELEMENT => null;
        end;
        Prepend_First_Element(List, 2);
        Trash := Get_Previous_Element(List);
        Put_Line("Get_Previous_Element OK");
        exception
            when others => Put_Line("Get_Previous_Element NOK");
    end;
    
    begin
        List := Create;
        begin
            Next(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Append_Last_Element(List, 1);
        begin
            Next(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_NO_MORE_ELEMENT => null;
        end;
        Append_Last_Element(List, 2);
        Next(List);
        Put_Line("Next OK");
        exception
            when others => Put_Line("Next NOK");
    end;

    begin
        List := Create;
        begin
            Previous(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Prepend_First_Element(List, 1);
        begin
            Previous(List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_NO_MORE_ELEMENT => null;
        end;
        Prepend_First_Element(List, 2);
        Previous(List);
        Put_Line("Previous OK");
        exception
            when others => Put_Line("Previous NOK");
    end;
    
    begin
        List := Create;
        Clear(List);
        if Get_Length(List) /= 0 then
            raise EXCEPTION_NOT_OK;
        end if;
        Prepend_First_Element(List, 1);
        Clear(List);
        if Get_Length(List) /= 0 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Clear OK");
        exception
            when others => Put_Line("Clear NOK");
    end;
    
    begin
        List := Create;
        begin
            Go_To(List, Get_First_Element(List));
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_LIST_EMPTY => null;
        end;
        Prepend_First_Element(List, 1);
        Go_To(List, Get_First_Element(List));
        if Get_Element(List) /= 1 then
            raise EXCEPTION_NOT_OK;
        end if;
        Append_Last_Element(List, 3);
        Go_To(List, Get_Last_Element(List));
        if Get_Element(List) /= 3 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Go_To OK");
        exception
            when others => Put_Line("Go_To NOK");
    end;
end Test_Double_Linked_List;
