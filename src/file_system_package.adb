with Ada.Calendar; use Ada.Calendar;
with Inode_Package; use Inode_Package;
with Memory_Block_Package; use Memory_Block_Package;
with Ada.Strings.Fixed;
with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Text_IO; use Ada.Text_IO;

package body File_System_Package is
    function Create return File_System is
        Allocator: Memory_Allocator;
        Inode_Create: Ptr_Inode;
        Root_File: Ptr_File;
    begin
        -- create an allocator and a root file
        Allocator := Create(FIRST_INDEX_HDD, SIZE_HDD);
        Inode_Create := Create(Folder, Allocate_Memory(Allocator, SIZE_INODE));
        Root_File := Create(ROOT_FILE_NAME, Inode_Create, NULL);
        return File_System'(Root_File => Root_File, Current_File => Root_File, Allocator => Allocator);
    end Create;


    function Pwd(File_System_Arg: in File_System; Args_List: in out Double_Linked_List) return String is
    begin
        if Get_Length(Args_List) > 0 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;
        -- return absolute path of the working directory
        return Get_Absolute_Path(File_System_Arg, File_System_Arg.Current_File);
    end Pwd;


    procedure Touch(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        Inode_To_Create: Ptr_Inode;
        File_To_Create: Ptr_File;
        File_Iterator: Ptr_File := File_System_Arg.Current_File;
        Path_List: Double_Linked_List;
    begin
        -- manage arguments
        if Get_Length(Args_List) /= 1 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        -- manage paths and find file 
        Path_List := Get_Path_List(To_String(Get_Element(Args_List)));
        for Index in 1..Get_Length(Path_List) - 1 loop
            File_Iterator := Get_Folder(File_System_Arg, File_Iterator, Get_Element(Path_List));
            Next(Path_List);
        end loop;

        begin
            -- if child exist then set modification date time of child by actual date time
            -- else create it (if enough memory)
            if not Has_Child(File_Iterator, Valid_Name(Get_Element(Path_List))) then
                Inode_To_Create := Create(Ordinary_File, Allocate_Memory(File_System_Arg.Allocator, SIZE_INODE));
                File_To_Create := Create(Get_Element(Path_List), Inode_To_Create, File_Iterator);
                Add_Child(File_Iterator, File_To_Create);
            else
                Set_Modification_Date_Time(Get_Inode(Get_File(File_System_Arg, File_Iterator, Get_Element(Path_List))), Clock);
            end if;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => 
                Set_Modification_Date_Time(Get_Inode(Get_File(File_System_Arg, File_Iterator, Get_Element(Path_List))), Clock);
            when EXCEPTION_NOT_ENOUGH_MEMORY => raise EXCEPTION_MEMORY_ERROR;
        end;
    end Touch;


    procedure Mkdir(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        Inode_To_Create: Ptr_Inode;
        File_To_Create: Ptr_File;
        File_Name: Unbounded_String;
        File_Iterator: Ptr_File := File_System_Arg.Current_File;
        Path_List: Double_Linked_List;
    begin
        -- manage argument
        if Get_Length(Args_List) /= 1 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        -- manage paths and find last folder which exists in the system
        Path_List := Get_Path_List(To_String(Get_Element(Args_List)));
        begin
            for Index in 1..Get_Length(Path_List) - 1 loop
                File_Iterator := Get_Folder(File_System_Arg, File_Iterator, Get_Element(Path_List));
                Next(Path_List);
            end loop;
            exception
                when EXCEPTION_FILE_NOT_FOUND => NULL;
        end;

        -- create all directory which not exist in the actual system (if enough memory)
        loop
            File_Name := Valid_Name(Get_Element(Path_List));
            Inode_To_Create := Create(Folder, Allocate_Memory(File_System_Arg.Allocator, SIZE_INODE));
            File_To_Create := Create(File_Name, Inode_To_Create, File_Iterator);
            Add_Child(File_Iterator, File_To_Create);
            File_Iterator := File_To_Create;
            exit when not Has_Next(Path_List);
            Next(Path_List);
        end loop;
    exception
        when EXCEPTION_NOT_ENOUGH_MEMORY => raise EXCEPTION_MEMORY_ERROR;
    end Mkdir;


    function Ls(File_System_Arg: in File_System; Args_List: in out Double_Linked_List) return String is
        File_Iterator: Ptr_File := File_System_Arg.Current_File;
        Path_List: Double_Linked_List;
        Ls_Recursively: Boolean;

        function Display_Recursively(File: in Ptr_File; Number_Of_Spaces: in Natural := 0; Recursively: in Boolean := False) return String is
            String: Unbounded_String := To_Unbounded_String("");
            Children_List: File_List_Package.Double_Linked_List;
            Child: Ptr_File;
        begin
            Children_List := Get_Children(File);
            if Get_Children_Size(File) > 0 then
                loop
                    Child := File_List_Package.Get_Element(Children_List);
                    for i in 1..Number_Of_Spaces loop
                        String := " " & String;
                    end loop;
                    String := String & Get_Name(Child) & " start index:" & Natural'image(Get_Start_Index(Get_Memory_Block(Get_Inode(Child)))) & " end index:" & Natural'image(Get_End_Index(Get_Memory_Block(Get_Inode(Child)))) & " physical size:" & Natural'image(Get_Physical_Size(Get_Memory_Block(Get_Inode(Child))));
                    if Recursively and Get_Children_Size(Child) > 0 then
                        String := String & LF;
                        String := String & Display_Recursively(Child, Number_Of_Spaces + 2, Recursively);
                    end if;
                    exit when not File_List_Package.Has_Next(Children_List);
                    String := String & LF;
                    File_List_Package.Next(Children_List);
                end loop;
            end if;
            return To_String(String);
        end Display_Recursively;
    begin
        -- manage arguments
        if Get_Length(Args_List) > 1 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        if Get_Length(Args_List) >= 1 and then Get_Element(Args_List) = "-r" then
            Ls_Recursively := True;
            Delete_Element(Args_List);
        else
            Ls_Recursively := False;
        end if;

        -- manage paths (if path is given) and find file with path
        -- display (recursively or not) the path
        if Get_Length(Args_List) = 0 then
            return Display_Recursively(File_Iterator, 0, Ls_Recursively);
        else
            Path_List := Get_Path_List(To_String(Get_Element(Args_List)));
            loop
                File_Iterator := Get_File(File_System_Arg, File_Iterator, Get_Element(Path_List));
                exit when not Has_Next(Path_List);
                Next(Path_List);
            end loop;
            return Display_Recursively(File_Iterator);
        end if;
    end Ls;


    procedure Cd(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        File_Iterator: Ptr_File := File_System_Arg.Current_File;
        Path_List: Double_Linked_List;
    begin
        -- manage arguments
        if Get_Length(Args_List) /= 1 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        -- manage paths and for each file, if the file is a folder, go into it
        Path_List := Get_Path_List(To_String(Get_Element(Args_List)));
        loop
            File_Iterator := Get_Folder(File_System_Arg, File_Iterator, Get_Element(Path_List));
            exit when not Has_Next(Path_List);
            Next(Path_List);
        end loop;
        File_System_Arg.Current_File := File_Iterator;
    end Cd;


    procedure Vim(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        File_To_Modify: Ptr_File;
        File_Iterator: Ptr_File := File_System_Arg.Current_File;
        Initial_Size: Natural;
        Path_List: Double_Linked_List;
    begin
        -- manage arguments
        if Get_Length(Args_List) /= 2 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        -- manage paths and find file to modify
        Path_List := Get_Path_List(To_String(Get_Element(Args_List)));
        for Index in 1..Get_Length(Path_List) - 1 loop
            File_Iterator := Get_Folder(File_System_Arg, File_Iterator, Get_Element(Path_List));
            Next(Path_List);
        end loop;
        File_To_Modify := Get_File(File_System_Arg, File_Iterator, Get_Element(Path_List));

        -- to change size, deallocate and reallocate memory
        begin
            if Get_Inode_Type(Get_Inode(File_To_Modify)) = Ordinary_File then
                Next(Args_List);
                begin
                    Initial_Size := Get_Physical_Size(Get_Memory_Block(Get_Inode(File_To_Modify)));
                    Deallocate_Memory(File_System_Arg.Allocator, Get_Memory_Block(Get_Inode(File_To_Modify)));
                    Set_Memory_Block(Get_Inode(File_To_Modify), Allocate_Memory(File_System_Arg.Allocator, SIZE_INODE + Natural'Value(To_String(Get_Element(Args_List)))));
                    exception
                        when CONSTRAINT_ERROR => 
                            Set_Memory_Block(Get_Inode(File_To_Modify), Allocate_Memory(File_System_Arg.Allocator, Initial_Size));
                            raise EXCEPTION_NOT_A_NUMERIC_VALUE;
                        when EXCEPTION_NOT_ENOUGH_MEMORY => raise EXCEPTION_MEMORY_ERROR;
                end;
            else
                raise EXCEPTION_NOT_AN_ORDINARY_FILE;
            end if;
        end;
    end Vim;


    procedure Rm(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        File_Iterator: Ptr_File := File_System_Arg.Current_File;
        File_To_Remove: Ptr_File;
        Path_List: Double_Linked_List;
        Recursive: Boolean;

        procedure Delete_Recursively(File_System_Arg: in out File_System; File: in Ptr_File) is
            Children: File_List_Package.Double_Linked_List;
        begin
            Children := Get_Children(File);
            if Get_Children_Size(File_To_Remove) > 0 then
                Children := Get_Children(File_To_Remove);
                loop
                    Delete_Recursively(File_System_Arg, File_List_Package.Get_Element(Children));
                    Delete_Child(File, File_List_Package.Get_Element(Children));
                    exit when not File_List_Package.Has_Next(Children);
                    File_List_Package.Next(Children);
                end loop;
            end if;
            Deallocate_Memory(File_System_Arg.Allocator, Get_Memory_Block(Get_Inode(File)));
            Delete_Child(Get_Parent(File), File);
        end Delete_Recursively;
    begin
        -- manage arguments
        if Get_Length(Args_List) < 1 and Get_Length(Args_List) > 3 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        if Get_Element(Args_List) = "-r" then
            if Get_Length(Args_List) < 2 then
                raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
            else
                Recursive := True;
                Delete_Element(Args_List);
            end if;
        else
            Recursive := False;
        end if;

        -- manage paths and find file to remove
        Path_List := Get_Path_List(To_String(Get_Element(Args_List)));
        for Index in 1..Get_Length(Path_List) - 1 loop
            File_Iterator := Get_Folder(File_System_Arg, File_Iterator, Get_Element(Path_List));
            Next(Path_List);
        end loop;
        File_To_Remove := Get_File(File_System_Arg, File_Iterator, Get_Element(Path_List));

        -- if the file to remove is the root file then error
        -- else if delete recursive and file to remove is a folder then delete it recursively
        -- else file to remove is not a folder then delete it
        -- else error
        begin
            if File_To_Remove = File_System_Arg.Root_File then
                raise EXCEPTION_CANNOT_CHANGE_ROOT;
            elsif Recursive or (not Recursive and Get_Inode_Type(Get_Inode(File_To_Remove)) /= Folder) then
                Delete_Recursively(File_System_Arg, File_To_Remove);
            else
                raise EXCEPTION_NOT_AN_ORDINARY_FILE;
            end if;
        exception
            when EXCEPTION_MEMORY_BLOCK_NOT_FOUND => raise EXCEPTION_MEMORY_ERROR;
        end;
    end Rm;


    procedure Mv(File_System_Arg: in File_System; Args_List: in out Double_Linked_List) is
        File_Iterator_Source: Ptr_File := File_System_Arg.Current_File;
        File_Iterator_Destination: Ptr_File := File_System_Arg.Current_File;
        File_To_Move: Ptr_File;
        Path_List_Source: Double_Linked_List;
        Path_List_Destination: Double_Linked_List;
        Absolute_Path_Source: Unbounded_String;
        Absolute_Path_Dest: Unbounded_String;
    begin
        -- manage arguments
        if Get_Length(Args_List) /= 2 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        -- manage paths and find file to move into destination folder
        Path_List_Source := Get_Path_List(To_String(Get_Element(Args_List)));
        Next(Args_List);
        Path_List_Destination := Get_Path_List(To_String(Get_Element(Args_List)));

        for Index in 1..Get_Length(Path_List_Source) - 1 loop
            File_Iterator_Source := Get_Folder(File_System_Arg, File_Iterator_Source, Get_Element(Path_List_Source));
            Next(Path_List_Source);
        end loop;
        File_To_Move := Get_File(File_System_Arg, File_Iterator_Source, Get_Element(Path_List_Source));
        if File_To_Move = File_System_Arg.Root_File then
            raise EXCEPTION_CANNOT_CHANGE_ROOT;
        end if;

        for Index in 1..Get_Length(Path_List_Destination) - 1 loop
            File_Iterator_Destination := Get_Folder(File_System_Arg, File_Iterator_Destination, Get_Element(Path_List_Destination));
            Next(Path_List_Destination);
        end loop;
        
        -- if first path is a sub-path of destination path then error
        -- else if file to move name already exist in destination folder then error
        -- else delete file to move from ancien parent and add file to move to the destination folder
        Absolute_Path_Source := To_Unbounded_String(Get_Absolute_Path(File_System_Arg, File_Iterator_Source));
        Absolute_Path_Dest := To_Unbounded_String(Get_Absolute_Path(File_System_Arg, File_Iterator_Destination));
        if Length(Absolute_Path_Source) /= Length(Absolute_Path_Dest) and then Ada.Strings.Fixed.Count(To_String(Absolute_Path_Dest), To_String(Absolute_Path_Source)) > 0 then
            raise EXCEPTION_PATH_IS_SUBPATH;
        elsif not Has_Child(File_Iterator_Destination, Valid_Name(Get_Element(Path_List_Destination))) then
            Set_Name(File_To_Move, Get_Element(Path_List_Destination));
            Delete_Child(Get_Parent(File_To_Move), File_To_Move);
            Add_Child(File_Iterator_Destination, File_To_Move);
        else
            raise EXCEPTION_FILE_ALREADY_EXIST;
        end if;
    end Mv;


    procedure Cp(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        File_Iterator_Source: Ptr_File := File_System_Arg.Current_File;
        File_Iterator_Destination: Ptr_File := File_System_Arg.Current_File;
        File_To_Copy: Ptr_File;
        File_Copy: Ptr_File;
        Path_List_Source: Double_Linked_List;
        Path_List_Destination: Double_Linked_List;
        Copy_Recursive: Boolean;

        function Cp_Recursive(File_System_Arg: in out File_System; File: in Ptr_File) return Ptr_File is
            Inode_Copy: Ptr_Inode;
            File_Copy: Ptr_File;
            Children: File_List_Package.Double_Linked_List;
        begin
            Inode_Copy := Create(Get_Inode_Type(Get_Inode(File)), Allocate_Memory(File_System_Arg.Allocator, Get_Physical_Size(Get_Memory_Block(Get_Inode(File)))));
            File_Copy := Create(Get_Name(File), Inode_Copy, NULL);
            if Get_Children_Size(File) > 0 then
                Children := Get_Children(File);
                loop
                    Add_Child(File_Copy, Cp_Recursive(File_System_Arg, File_List_Package.Get_Element(Children)));
                    exit when not File_List_Package.Has_Next(Children);
                    File_List_Package.Next(Children);
                end loop;
            end if;
            return File_Copy;
        end Cp_Recursive;
    begin
        -- manage arguments
        if Get_Length(Args_List) < 2 and Get_Length(Args_List) > 4 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        if Get_Element(Args_List) = "-r" then
            Copy_Recursive := True;
            Delete_Element(Args_List);
        else
            Copy_Recursive := False;
        end if;

        -- manage paths and find file to copy and destination folder
        Path_List_Source := Get_Path_List(To_String(Get_Element(Args_List)));
        for Index in 1..Get_Length(Path_List_Source) - 1 loop
            File_Iterator_Source := Get_Folder(File_System_Arg, File_Iterator_Source, Get_Element(Path_List_Source));
            Next(Path_List_Source);
        end loop;
        File_To_Copy := Get_File(File_System_Arg, File_Iterator_Source, Get_Element(Path_List_Source));
        if Get_Inode_Type(Get_Inode(File_To_Copy)) = Folder and not Copy_Recursive then
            raise EXCEPTION_NOT_AN_ORDINARY_FILE;
        end if;

        Path_List_Destination := Get_Path_List(To_String(Get_Last_Element(Args_List)));
        for Index in 1..Get_Length(Path_List_Destination) - 1 loop
            File_Iterator_Destination := Get_Folder(File_System_Arg, File_Iterator_Destination, Get_Element(Path_List_Destination));
            Next(Path_List_Destination);
        end loop;

        -- if file does not exists in destination folder, copy it into destination folder (if enough memory)
        begin
            if not Has_Child(File_Iterator_Destination, Valid_Name(Get_Element(Path_List_Destination))) then
                File_Copy := Cp_Recursive(File_System_Arg, File_To_Copy);
                Set_Name(File_Copy, Get_Element(Path_List_Destination));
                Add_Child(File_Iterator_Destination, File_Copy);
            else
                raise EXCEPTION_FILE_ALREADY_EXIST;
            end if;
        exception
            when EXCEPTION_NOT_ENOUGH_MEMORY => raise EXCEPTION_MEMORY_ERROR;
        end;
    end Cp;


    procedure Defragment(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
    begin
        -- manage arguments
        if Get_Length(Args_List) /= 0 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;
        -- run rearrange memory from memory package
        Rearrange_Memory(File_System_Arg.Allocator);
    end Defragment;


    procedure Tar(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List) is
        File_Iterator_Source: Ptr_File := File_System_Arg.Current_File;
        File_Iterator_Destination: Ptr_File := File_System_Arg.Current_File;
        Inode: Ptr_Inode;
        Archive_Size: Natural;
        File_Archive: Ptr_File;
        Path_List_Source: Double_Linked_List;
        Path_List_Destination: Double_Linked_List;
    begin
        -- manage arguments
        if Get_Length(Args_List) < 2 or Get_Length(Args_List) > 3 then
            raise EXCEPTION_INVALID_NUMBER_OF_ARGS;
        end if;

        -- manage paths and find file to archive and destination folder
        Path_List_Source := Get_Path_List(To_String(Get_Element(Args_List)));
        for Index in 1..Get_Length(Path_List_Source) - 1 loop
            File_Iterator_Source := Get_Folder(File_System_Arg, File_Iterator_Source, Get_Element(Path_List_Source));
            Next(Path_List_Source);
        end loop;

        Path_List_Destination := Get_Path_List(To_String(Get_Next_Element(Args_List)));
        for Index in 1..Get_Length(Path_List_Destination) - 1 loop
            File_Iterator_Destination := Get_Folder(File_System_Arg, File_Iterator_Destination, Get_Element(Path_List_Destination));
            Next(Path_List_Destination);
        end loop;

        -- if file does not exists in destination folder, archive it into destination folder
        begin
            if not Has_Child(File_Iterator_Destination, Valid_Name(Get_Element(Path_List_Destination) & ".tar")) then
                Archive_Size := Get_Total_Physical_Size(Get_File(File_System_Arg, File_Iterator_Source, Get_Element(Path_List_Source)));
                Inode := Create(Ordinary_File, Allocate_Memory(File_System_Arg.Allocator, Archive_Size));
                File_Archive := Create(Get_Element(Path_List_Destination) & ".tar", Inode, File_Iterator_Destination);
                Add_Child(File_Iterator_Destination, File_Archive);
            else
                raise EXCEPTION_FILE_ALREADY_EXIST;
            end if;
        exception
            when EXCEPTION_NOT_ENOUGH_MEMORY => raise EXCEPTION_MEMORY_ERROR;
        end;
    end Tar;


    function Split_String(String_To_Split: in String; Separator: in String; Escape: in String := "\") return Double_Linked_List is
        Result_Split_List: Double_Linked_List := Create;
        Min_String_Index: Natural := String_To_Split'First + Separator'Length - 1;
        Max_String_Index: constant Natural := String_To_Split'Last - Separator'Length + 1;
        Separator_Boolean: Boolean;
        Escape_Boolean: Boolean;
    begin
        -- iterate through the string
        -- if a separator is find in the string then
        --      if a escape is before then continue to iterate
        --      else add the sub string to a list
        -- return the list
        for Current_Index in Min_String_Index..Max_String_Index loop
            Separator_Boolean := String_To_Split(Current_Index..(Current_Index + Separator'Length - 1)) = Separator;
            Escape_Boolean := Current_Index - Escape'Length > String_To_Split'First and then String_To_Split((Current_Index - Escape'Length)..(Current_Index - 1)) = Escape;

            if Separator_Boolean and then not Escape_Boolean then
                if String_To_Split(Min_String_Index..(Current_Index - 1)) /= "" then
                    Append_Last_Element(Result_Split_List, To_Unbounded_String(String_To_Split(Min_String_Index..(Current_Index - 1))));
                    Min_String_Index := Current_Index + Separator'Length;
                else
                    Min_String_Index := Current_Index + 1;
                end if;
            end if;
        end loop;
        if String_To_Split(Min_String_Index..String_To_Split'Last) /= "" then
            Append_Last_Element(Result_Split_List, To_Unbounded_String(String_To_Split(Min_String_Index..String_To_Split'Last)));
        end if;
        return Result_Split_List;
    end Split_String;


    function Neutralize_Escape(String_To_Clean: in String; Escape: in String := "\") return String is
    begin
        -- if the string is empty then return it
        if String_To_Clean = "" then
            return String_To_Clean;
        -- else if the string = escape then
        --      if the string after the escape as an escape then return escape concatenate to neutralize escape on the end of the string
        --      else return neutralize escape on the end of the string
        -- else return the first character and neutralize the end of the string
        elsif String_To_Clean'First + Escape'Length - 1 <= String_To_Clean'Last and then String_To_Clean(String_To_Clean'First..(String_To_Clean'First + Escape'Length - 1)) = Escape then
            if String_To_Clean'First + 2 * Escape'Length - 1 <= String_To_Clean'Last and then String_To_Clean((String_To_Clean'First + Escape'Length)..(String_To_Clean'First + 2 * Escape'Length - 1)) = Escape then
                return Escape & Neutralize_Escape(String_To_Clean((String_To_Clean'First + 2 * Escape'Length)..String_To_Clean'Last));
            else
                return Neutralize_Escape(String_To_Clean((String_To_Clean'First + Escape'Length)..String_To_Clean'Last));
            end if;
        else
            return String_To_Clean(String_To_Clean'First) & Neutralize_Escape(String_To_Clean((String_To_Clean'First + 1)..String_To_Clean'Last));
        end if;
    end Neutralize_Escape;


    function Get_Path_List(String_To_Split: in String) return Double_Linked_List is
        Path_List: Double_Linked_List := Create;
    begin
        -- split string by "/" but if the first character is a "/" then add it first (otherwise it will be delete by split because it is the separator
        if String_To_Split'Length >= 1 then
            if String_To_Split(String_To_Split'First) = '/' then
                Path_List := Split_String(String_To_Split(String_To_Split'First + 1..String_To_Split'Last), "/");
                Prepend_First_Element(Path_List, To_Unbounded_String("/"));
                Go_To(Path_List, Get_First_Element(Path_List));
            else
                Path_List := Split_String(String_To_Split, "/");
            end if;
            return Path_List;
        else
            Append_Element(Path_List, To_Unbounded_String(""));
        end if;
        return Path_List;
    end Get_Path_List;


    function Get_Absolute_Path(File_System_Arg: in File_System; File: in Ptr_File) return String is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NOT_FOUND;
        -- if file is the root file then return name
        elsif Get_Parent(File) = NULL then
            return To_String(ROOT_FILE_NAME);
        -- else return absolute path of the file concatenate with the name of the name concanate with "/" (for separation)
        else
            return Get_Absolute_Path(File_System_Arg, Get_Parent(File)) & To_String(Get_Name(File)) & "/";
        end if;
    end Get_Absolute_Path;


    function Get_Total_Physical_Size(File: in Ptr_File) return Natural is
        Children: File_List_Package.Double_Linked_List;
        Physical_Size: Natural;
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NOT_FOUND;
        end if;
        -- physical size take physical size of the file sent as parameter
        Physical_Size := Get_Physical_Size(Get_Memory_Block(Get_Inode(File)));
        Children := Get_Children(File);
        -- for each children, physical size is increased by total physical size of the children 
        if Get_Children_Size(File) > 0 then
            loop
                Physical_Size := Physical_Size + Get_Total_Physical_Size(File_List_Package.Get_Element(Children));
                exit when not File_List_Package.Has_Next(Children);
                File_List_Package.Next(Children);
            end loop;
        end if;
        return Physical_Size;
    end Get_Total_Physical_Size;


    function Get_File(File_System_Arg: in File_System; File_Iterator: in Ptr_File; Name: in Unbounded_String) return Ptr_File is
    begin
        -- if name is ".." then return parent
        -- else if name is "." then return itself
        -- else if name is "/" then return root folder
        -- else find child by name from file (if it exists else error)
        if Name = ROOT_FILE_NAME then
            return File_System_Arg.Root_File;
        elsif Name = PARENT_FILE_NAME and File_Iterator = File_System_Arg.Root_File then
            return File_Iterator;
        elsif Name = PARENT_FILE_NAME then
            return Get_Parent(File_Iterator);
        elsif Name = CURRENT_FILE_NAME then
            return File_Iterator;
        else
            return Get_Child(File_Iterator, Name);
        end if;

        exception
            when EXCEPTION_FILE_CHILD_NOT_FOUND => raise EXCEPTION_FILE_NOT_FOUND;
    end Get_File;


    function Get_Folder(File_System_Arg: in File_System; File_Iterator: in Ptr_File; Name: in Unbounded_String) return Ptr_File is
        Result: Ptr_File;
    begin
        -- get file and if file return by get file is a folder then return it else error
        Result := Get_File(File_System_Arg, File_Iterator, Name);
        if Get_Inode_Type(Get_Inode(Result)) = Folder then
            return Result;
        else
            raise EXCEPTION_NOT_A_FOLDER;
        end if;
    end Get_Folder;


    function Valid_Name(Name: in Unbounded_String) return Unbounded_String is
    begin
        -- if name is not "." or ".." or "/" then it's a valid name else error
        if Name = CURRENT_FILE_NAME or Name = PARENT_FILE_NAME or Name = ROOT_FILE_NAME then
            raise EXCEPTION_FILE_ALREADY_EXIST;
        else
            return Name;
        end if;
    end Valid_Name;
end File_System_Package;
