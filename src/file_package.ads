with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Inode_Package; use Inode_Package;
with Double_Linked_List_Package;

package File_Package is
    type File is private;
    type Ptr_File is access File;

    EXCEPTION_FILE_NULL: Exception;
    EXCEPTION_FILE_CHILD_NOT_FOUND: Exception;
    EXCEPTION_FILE_CHILD_ALREADY_EXIST: Exception;

    package File_List_Package is new Double_Linked_List_Package(Element_Type => Ptr_File, "=" => "=");
    use File_List_Package;

    -- Name: Create
    -- Semantics: Create a new Ptr_File if Ptr_Inode is not NULL.
    -- Parameters:
    --      Name: Name of the file.
    --      Inode: Ptr_Inode to assign to file.
    --      Parent: Ptr_File to assign to file.
    -- Return type: Ptr_File
    -- Exceptions: EXCEPTION_FILE_NULL,
    -- Preconditions: ~
    -- Postconditions: ~
    function Create(Name: in Unbounded_String; Inode: in Ptr_Inode; Parent: in Ptr_File) return Ptr_File;
    
    -- Name: Get_Name
    -- Semantics: Return the name of the Ptr_File sent as parameter. 
    -- Parameters:
    --      File: Ptr_File from which we want the name.
    -- Return type: Ada.String.Unbounded.Unbounded_String
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Name(File: in Ptr_File) return Unbounded_String;

    -- Name: Get_Inode
    -- Semantics: Return the inode of the Ptr_File sent as parameter. 
    -- Parameters:
    --      File: Ptr_File from which we want the inode.
    -- Return type: Ptr_Inode
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Inode(File: in Ptr_File) return Ptr_Inode;

    -- Name: Get_Parent
    -- Semantics: Return the parent of the Ptr_File sent as parameter. 
    -- Parameters:
    --      File: Ptr_File from which we want the parent.
    -- Return type: Ptr_File
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Parent(File: in Ptr_File) return Ptr_File;

    -- Name: Get_Children_Size
    -- Semantics: Return the children list's length of the Ptr_File sent as parameter. 
    -- Parameters:
    --      File: Ptr_File from which we want the children list's size.
    -- Return type: Natural
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Children_Size(File: in Ptr_File) return Natural;

    -- Name: Get_Children
    -- Semantics: Return the children list of the Ptr_File sent as parameter. 
    -- Parameters:
    --      File: Ptr_File from which we want the children list.
    -- Return type: Double_Linked_List
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Children(File: in Ptr_File) return Double_Linked_List;

    -- Name: Has_Child
    -- Semantics: Return an boolean in order to advertise if a child with the name sent as parameter exists.
    -- Parameters:
    --      File: Ptr_File from which we want to know if a child exists.
    -- Return type: Boolean
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: True if a child with the name sent as parameter exists else False.
    function Has_Child(File: in Ptr_File; Name: in Unbounded_String) return Boolean;

    -- Name: Get_Child
    -- Semantics: Return the Ptr_File's child with the name sent as parameter if it exists. 
    -- Parameters:
    --      File: Ptr_File from which we want the child if it exists.
    -- Return type: Ptr_File
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Child(File: in Ptr_File; Name: in Unbounded_String) return Ptr_File;

    -- Name: Set_Name
    -- Semantics: Change the Ptr_File's name.
    -- Parameters:
    --      File: Ptr_File from which we want to change the name.
    --      Name: New file name.
    -- Exceptions: EXCEPTION_FILE_NULL
    -- Preconditions: ~
    -- Postconditions: File.Name = Name
    procedure Set_Name(File: in Ptr_File; Name: in Unbounded_String);

    -- Name: Add_Child
    -- Semantics: Add a child to the Ptr_File's children list if the child name is unique in the list. Sorted add by name in ascending order.
    -- Parameters:
    --      File: Ptr_File from which we want to add a child.
    --      Child_To_Add: Child to add to the Ptr_File's children list.
    -- Exceptions: EXCEPTION_FILE_NULL, EXCEPTION_FILE_CHILD_ALREADY_EXIST
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Add_Child(File: in Ptr_File; Child_To_Add: in Ptr_File);

    -- Name: Delete_Child
    -- Semantics: Delete a child to the Ptr_File's children list if it exists.
    -- Parameters:
    --      File: Ptr_File from which we want to change the name.
    --      Child_To_Delete: Child to delete to the Ptr_File's children list.
    -- Exceptions: EXCEPTION_FILE_NULL, EXCEPTION_FILE_CHILD_NOT_FOUND
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Delete_Child(File: in Ptr_File; Child_To_Delete: in Ptr_File);
private
    type File is record
        Name: Unbounded_String;
        Inode: Ptr_Inode;
        Parent: Ptr_File;
        Children: Double_Linked_List;
    end record;
end File_Package;
