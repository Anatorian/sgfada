with Ada.Text_IO; use Ada.Text_IO;
with Inode_Package; use Inode_Package;
with Memory_Block_Package; use Memory_Block_Package;
with File_Package; use File_Package;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

procedure Test_File is
    EXCEPTION_NOT_OK: Exception;

    Inode: Ptr_Inode := Create(Ordinary_File, Create(1, 2));
    Children: File_List_Package.Double_Linked_List;
    File: Ptr_File;
begin
    begin
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    Put_Line("Create OK");
    exception
        when others => Put_Line("Create NOK");
    end;

    begin
    File := NULL;
    begin
        if Get_Name(File) = "File" then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    if Get_Name(File) /= "File" then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Name OK");
    exception
        when others => Put_Line("Get_Name NOK");
    end;
    
    begin
    File := NULL;
    begin
        if Get_Inode(File) = Inode then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    if Get_Inode(File) /= Inode then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Inode OK");
    exception
        when others => Put_Line("Get_Inode NOK");
    end;

    begin
    File := NULL;
    begin
        if Get_Parent(File) = NULL then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    if Get_Parent(File) /= NULL then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Parent OK");
    exception
        when others => Put_Line("Get_Parent NOK");
    end;

    begin
    File := NULL;
    begin
        if Get_Children_Size(File) = 0 then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    if Get_Children_Size(File) /= 0 then
        raise EXCEPTION_NOT_OK;
    end if;
    Add_Child(File, File);
    if Get_Children_Size(File) /= 1 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Children_Size OK");
    exception
        when others => Put_Line("Get_Children_Size NOK");
    end;

    begin
    File := NULL;
    begin
        Children := Get_Children(File);
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    Children := Get_Children(File);
    Put_Line("Get_Children OK");
    exception
        when others => Put_Line("Get_Children NOK");
    end;

    begin
    File := NULL;
    begin
        if Get_Name(File) = "fezi" then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    if Get_Name(File) /= "File" then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Child OK");
    exception
        when others => Put_Line("Get_Child NOK");
    end;

    begin
    File := NULL;
    begin
        if Get_Name(File) = "fezi" then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    if Get_Name(File) /= "File" then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Set_Name OK");
    exception
        when others => Put_Line("Set_Name NOK");
    end;

    begin
    File := NULL;
    begin
        Set_Parent(File, File);
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    Set_Parent(File, File);
    if Get_Parent(File) /= File then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Set_Parent OK");
    exception
        when others => Put_Line("Set_Parent NOK");
    end;

    begin
    File := NULL;
    begin
        Add_Child(File, File);
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    Add_Child(File, File);
    if Get_Children_Size(File) /= 1 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Add_Child OK");
    exception
        when others => Put_Line("Add_Child NOK");
    end;

    begin
    File := NULL;
    begin
        Delete_Child(File, File);
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NULL => NULL;
    end;
    File := Create(To_Unbounded_String("File"), Inode, NULL);
    Add_Child(File, File);
    Delete_Child(File, File);
    Put_Line("Delete_Child OK");
    exception
        when others => Put_Line("Delete_Child NOK");
    end;
end Test_File;
