with Memory_Block_Package; use Memory_Block_Package;
with Ada.Text_IO; use Ada.Text_IO;

procedure Test_Memory_Block is
    EXCEPTION_NOT_OK: Exception;

    Memory_Block: Ptr_Memory_Block;
begin
    begin
    Memory_Block := Create(1, 2);
    Put_Line("Create OK");
    exception
        when others => Put_Line("Create NOK");
    end;

    begin
    Memory_Block := NULL;
    begin
        if Get_Start_Index(Memory_Block) = 10 then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_MEMORY_BLOCK_NULL => NULL;
    end;
    Memory_Block := Create(1, 2);
    if Get_Start_Index(Memory_Block) /= 1 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Start_Index OK");
    exception
        when others => Put_Line("Get_Start_Index NOK");
    end;

    begin
    Memory_Block := NULL;
    begin
        if Get_End_Index(Memory_Block) = 10 then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_MEMORY_BLOCK_NULL => NULL;
    end;
    Memory_Block := Create(1, 2);
    if Get_End_Index(Memory_Block) /= 2 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_End_Index OK");
    exception
        when others => Put_Line("Get_End_Index NOK");
    end;

    begin
    Memory_Block := NULL;
    begin
        if Get_Physical_Size(Memory_Block) = 10 then
            NULL;
        end if;
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_MEMORY_BLOCK_NULL => NULL;
    end;
    Memory_Block := Create(1, 2);
    if Get_Physical_Size(Memory_Block) /= 2 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Get_Physical_Size OK");
    exception
        when others => Put_Line("Get_Physical_Size NOK");
    end;

    begin
    Memory_Block := NULL;
    begin
        Set_Start_Index(Memory_Block, 10);
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_MEMORY_BLOCK_NULL => NULL;
    end;
    Memory_Block := Create(1, 2);
    Set_Start_Index(Memory_Block, 10);
    if Get_Start_Index(Memory_Block) /= 10 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Set_Start_Index OK");
    exception
        when others => Put_Line("Set_Start_Index NOK");
    end;

    begin
    Memory_Block := NULL;
    begin
        Set_Physical_Size(Memory_Block, 10);
        raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_MEMORY_BLOCK_NULL => NULL;
    end;
    Memory_Block := Create(1, 2);
    Set_Physical_Size(Memory_Block, 10);
    if Get_Physical_Size(Memory_Block) /= 10 then
        raise EXCEPTION_NOT_OK;
    end if;
    Put_Line("Set_Physical_Size OK");
    exception
        when others => Put_Line("Set_Physical_Size NOK");
    end;
end Test_Memory_Block;
