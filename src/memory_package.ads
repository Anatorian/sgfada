with Memory_Block_Package; use Memory_Block_Package;
with Double_Linked_List_Package;

package Memory_Package is
    type Memory_Allocator is private;

    EXCEPTION_NOT_ENOUGH_MEMORY: Exception;
    EXCEPTION_MEMORY_BLOCK_NOT_FOUND: Exception;

    -- Name: Create
    -- Semantics: Create a new Memory_Allocator.
    -- Parameters:
    --      Start_Index: start index from which the memory allocator start.
    --      Total_Size: total size which the memory allocator process.
    -- Return type: Memory_Allocator
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Create(Start_Index: in Natural; Total_Size: in Natural) return Memory_Allocator;

    -- Name: Allocate_Memory
    -- Semantics: Allocate memory for a new memory block and return it if enough memory.
    -- Parameters:
    --      Allocator: Memory_Allocator use to allocate memory.
    --      Physical_Size: Size of the new memory block allocated.
    -- Return type: Ptr_Memory_Block
    -- Exceptions: EXCEPTION_NOT_ENOUGH_MEMORY
    -- Preconditions: ~
    -- Postconditions: ~
    function Allocate_Memory(Allocator: in out Memory_Allocator; Physical_Size: in Natural) return Ptr_Memory_Block;

    -- Name: Deallocate_Memory
    -- Semantics: Deallocate memory block.
    -- Parameters:
    --      Allocator: Memory_Allocator use to deallocate memory block.
    --      Memory_Block: memory block to deallocate.
    -- Exceptions: EXCEPTION_MEMORY_BLOCK_NOT_FOUND
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Deallocate_Memory(Allocator: in out Memory_Allocator; Memory_Block: in Ptr_Memory_Block);

    -- Name: Rearrange_Memory
    -- Semantics: Reassign all memory blocks's start index to the minimum start index possible.
    -- Parameters:
    --      Allocator: Memory_Allocator use to rearrange memory.
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Rearrange_Memory(Allocator: in out Memory_Allocator);
private
    package Free_Spaces_List_Package is new Double_Linked_List_Package(Element_Type => Ptr_Memory_Block, "=" => "=");
    use Free_Spaces_List_Package;

    type Memory_Allocator is record
        Start_Index: Natural;
        Total_Size: Natural;
        Memory_Blocks_Free: Double_Linked_List;
        Memory_Blocks_Used: Double_Linked_List;
    end record;

    -- Name: Merge_Memory
    -- Semantics: Iterate throught all memory blocks of the memory block free list and try to merge then if possible.
    -- Parameters:
    --      Allocator: Memory_Allocator use to merge memory.
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Merge_Memory(Allocator: in out Memory_Allocator);
end Memory_Package;
