with Ada.Strings.Maps.Constants; use Ada.Strings.Maps.Constants;

package body File_Package is
    function Create(Name: in Unbounded_String; Inode: in Ptr_Inode; Parent: in Ptr_File) return Ptr_File is
    begin
        if Inode = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            return new File'(Name, Inode, Parent, Create);
        end if;
    end Create;

    function Get_Name(File: in Ptr_File) return Unbounded_String is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            return File.Name;
        end if;
    end Get_Name;

    function Get_Inode(File: in Ptr_File) return Ptr_Inode is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            return File.Inode;
        end if;
    end Get_Inode;

    function Get_Parent(File: in Ptr_File) return Ptr_File is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            return File.Parent;
        end if;        
    end Get_Parent;

    function Get_Children_Size(File: in Ptr_File) return Natural is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            return Get_Length(File.Children);
        end if;
    end Get_Children_Size;

    function Get_Children(File: in Ptr_File) return Double_Linked_List is
        Children_List: Double_Linked_List := Create;
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        -- make a copy of the children list and return it
        elsif Get_Length(File.Children) > 0 then
            Go_To(File.Children, Get_First_Element(File.Children));
            loop
                Append_Last_Element(Children_List, Get_Element(File.Children));
                exit when not Has_Next(File.Children);
                Next(File.Children);
            end loop;
        end if;
        return Children_List;
    end Get_Children;

    function Has_Child(File: in Ptr_File; Name: in Unbounded_String) return Boolean is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        -- if children list is empty then
        --      return false
        -- else
        --      while children list has element and child name /= name loop
        --          iterate through children list
        --      end loop
        --      return child name = name
        -- end if
        elsif Get_Length(File.Children) = 0 then
            return False;
        else
            Go_To(File.Children, Get_First_Element(File.Children));
            while Get_Element(File.Children) /= Get_Last_Element(File.Children) and Get_Name(Get_Element(File.Children)) /= Name loop
                Next(File.Children);
            end loop;

            return Get_Name(Get_Element(File.Children)) = Name;
        end if;
    end Has_Child;

    function Get_Child(File: in Ptr_File; Name: in Unbounded_String) return Ptr_File is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        -- if children list is empty then error
        elsif Get_Length(File.Children) = 0 then
            raise EXCEPTION_FILE_CHILD_NOT_FOUND;
        else
            --   while children list has element and child name /= name loop
            --          iterate through children list
            --      end loop
            Go_To(File.Children, Get_First_Element(File.Children));
            while Get_Element(File.Children) /= Get_Last_Element(File.Children) and Get_Name(Get_Element(File.Children)) /= Name loop
                Next(File.Children);
            end loop;

            -- if child name = name then
            --      return child
            -- else
            --      error
            -- end if
            if Get_Name(Get_Element(File.Children)) /= Name then
                raise EXCEPTION_FILE_CHILD_NOT_FOUND;
            else
                return Get_Element(File.Children);
            end if;
        end if;
    end Get_Child;

    procedure Set_Name(File: in Ptr_File; Name: in Unbounded_String) is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            File.Name := Name;
        end if;
    end Set_Name;

    procedure Add_Child(File: in Ptr_File; Child_To_Add: in Ptr_File) is
        Lower_Case_Child: Unbounded_String;
        Lower_Case_Child_To_Add: Unbounded_String; 
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        -- if child list is empty then add element
        elsif Get_Length(File.Children) = 0 then
            Append_Last_Element(File.Children, Child_To_Add);
        -- else
        --      iterate through the list and get current child
        --      if child name (lower case) = name (lower case) then error
        --      else if child name > name then append
        --      else if child name < name then prepend
        --      else error
        else
            Lower_Case_Child_To_Add := Translate(Get_Name(Child_To_Add), Lower_Case_Map);
            Go_To(File.Children, Get_First_Element(File.Children));
            loop
                Lower_Case_Child := Translate(Get_Name(Get_Element(File.Children)), Lower_Case_Map);
                exit when not Has_Next(File.Children) or Lower_Case_Child >= Lower_Case_Child_To_Add;
                Next(File.Children);
            end loop;

            if Lower_Case_Child = Lower_Case_Child_To_Add then
                if Get_Name(Get_Element(File.Children)) = Get_Name(Child_To_Add) then
                    raise EXCEPTION_FILE_CHILD_ALREADY_EXIST;
                elsif Get_Name(Get_Element(File.Children)) > Get_Name(Child_To_Add) then
                    Append_Element(File.Children, Child_To_Add);
                else
                    Prepend_Element(File.Children, Child_To_Add);
                end if;
            else
                Prepend_Element(File.Children, Child_To_Add);
            end if;
        end if;
        Child_To_Add.Parent := File;
    end Add_Child;

    procedure Delete_Child(File: in Ptr_File; Child_To_Delete: in Ptr_File) is
    begin
        if File = NULL then
            raise EXCEPTION_FILE_NULL;
        else
            -- go to child and delete it if it exists else error
            Go_To(File.Children, Child_To_Delete);
            Delete_Element(File.Children);
        end if;
        exception
            when EXCEPTION_LIST_ELEMENT_NOT_FOUND => raise EXCEPTION_FILE_CHILD_NOT_FOUND;
    end Delete_Child;
end File_Package;
