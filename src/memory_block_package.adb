package body Memory_Block_Package is
    function Create(Start_Index: in Natural; Physical_Size: in Natural) return Ptr_Memory_Block is
    begin
        return new Memory_Block'(Start_Index, Physical_Size);
    end Create;


    function Get_Start_Index(Memory_Block: in Ptr_Memory_Block) return Natural is
    begin
        if Memory_Block = NULL then
            raise EXCEPTION_MEMORY_BLOCK_NULL;
        else
            return Memory_Block.Start_Index;
        end if;
    end Get_Start_Index;


    function Get_End_Index(Memory_Block: in Ptr_Memory_Block) return Natural is
    begin
        if Memory_Block = NULL then
            raise EXCEPTION_MEMORY_BLOCK_NULL;
        else
            return Memory_Block.Start_Index + Memory_Block.Physical_Size - 1;
        end if;
    end Get_End_Index;


    function Get_Physical_Size(Memory_Block: in Ptr_Memory_Block) return Natural is
    begin
        if Memory_Block = NULL then
            raise EXCEPTION_MEMORY_BLOCK_NULL;
        else
            return Memory_Block.Physical_Size;
        end if;
    end Get_Physical_Size;


    procedure Set_Start_Index(Memory_Block: in Ptr_Memory_Block; Start_Index: in Natural) is
    begin
        if Memory_Block = NULL then
            raise EXCEPTION_MEMORY_BLOCK_NULL;
        else
            Memory_Block.Start_Index := Start_Index;
        end if;
    end Set_Start_Index;


    procedure Set_Physical_Size(Memory_Block: in Ptr_Memory_Block; Physical_Size: in Natural) is
    begin
        if Memory_Block = NULL then
            raise EXCEPTION_MEMORY_BLOCK_NULL;
        else
            Memory_Block.Physical_Size := Physical_Size;
        end if;
    end Set_Physical_Size;
end Memory_Block_Package;
