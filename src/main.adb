with Ada.Text_IO; use Ada.Text_IO;
with Ada.Characters.Latin_1; use Ada.Characters.Latin_1; 
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Fixed;
with File_System_Package; use File_System_Package;

procedure Main is
    System: File_System := Create;
    Args_List: Unbounded_String_List.Double_Linked_List := Unbounded_String_List.Create;
    Command: Unbounded_String;
    Menu_Active: Boolean := False;
    Trash_Character: Character;

    function Menu return String is
        Choice: Natural;
        Bad_Choice: Boolean := False;
        Command: Unbounded_String;
    begin
        loop
            begin
                Put_Line(ESC & "[2J");
                if Bad_Choice then
                    Put_Line("Bad choice, retry.");
                end if;
                Put_Line("1 - Get working directory");
                Put_Line("2 - Create file");
                Put_Line("3 - Create directory");
                Put_Line("4 - Delete file/directory");
                Put_Line("5 - Change directory");
                Put_Line("6 - Copy file/directory");
                Put_Line("7 - Display directory");
                Put_Line("8 - Compress directory");
                Put_Line("9 - Change file physical size");
                Put_Line("10 - Defragment disk");
                Put_Line("11 - Move File/Directory");
                Put_Line("12 - Exit");
                Put("Choice: ");
                Choice := Natural'value(Get_Line);
                exception
                    when CONSTRAINT_ERROR => Bad_Choice := True;
            end;
            exit when Choice >= 1 and Choice <= 11;
            Bad_Choice := True;
        end loop;

        case Choice is
            when 1 => 
                return "pwd";
            when 2 =>
                Put("Path of file: ");
                return "touch " & Get_Line;
            when 3 =>
                Put("Path of directory: ");
                return "mkdir " & Get_Line;
            when 4 =>
                Put("Path of file/directory to delete: ");
                return "rm -r " & Get_Line;
            when 5 =>
                Put("Change current path to path: ");
                return "cd " & Get_Line;
            when 6 =>
                Command := To_Unbounded_String("cp "); 
                Put("Copy file/directory from path: ");
                Command := Command & Get_Line;
                Put("to path: ");
                return To_String(Command & " " & Get_Line);
            when 7 => 
                Put("Path of directory: ");
                return "ls " & Get_Line;
            when 8 => 
                Command := To_Unbounded_String("tar ");
                Put("File/Directory to archive: ");
                Command := Command & Get_Line;
                Put("Archive's path: ");
                return To_String(Command & " " & Get_Line);
            when 9 => 
                Command := To_Unbounded_String("vim ");
                Put("Change file: ");
                Command := Command &  Get_Line;
                Put("to physical size of: ");
                return To_String(Command & " " & Get_Line);
            when 10 => 
                return "defragment";
            when 11 =>
                Command := To_Unbounded_String("mv ");
                Put("Move file/directory: ");
                Command := Command &  Get_Line;
                Put("to: ");
                return To_String(Command & " " & Get_Line);
            when 12 => 
                Menu_Active := False;
                return "";
            when others =>
                return "";
        end case;
    end menu;

    procedure Process_Command(System: in out File_System; Args_List: in out Unbounded_String_List.Double_Linked_List; Menu_Active: in out Boolean) is
        Command: Unbounded_String;
    begin
        Command := Unbounded_String_List.Get_Element(Args_List);
        Unbounded_String_List.Delete_Element(Args_List);
        if Command = "pwd" then
            Put_Line(Pwd(System, Args_List));
        elsif Command = "touch" then
            Touch(System, Args_List);
        elsif Command = "mkdir" then
            Mkdir(System, Args_List);
        elsif Command = "ls" then
            Put_Line(Ls(System, Args_List));
        elsif Command = "cd" then
            Cd(System, Args_List);
        elsif Command = "vim" then
            Vim(System, Args_List);
        elsif Command = "rm" then
            Rm(System, Args_List);
        elsif Command = "mv" then
            Mv(System, Args_List);
        elsif Command = "cp" then
            Cp(System, Args_List);
        elsif Command = "defragment" then
            Defragment(System, Args_List);
        elsif Command = "tar" then
            Tar(System, Args_List);
        elsif Command = "menu" then
            Menu_Active := True;
        else
            Put_Line(To_String(Command) & ": command was not found.");
        end if;
    end Process_Command;
begin
    while True loop
        begin
            Unbounded_String_List.Clear(Args_List);
            if Menu_Active then
                Command := To_Unbounded_String(Menu);
                if Command /= "" then
                    Args_List := Split_String(Neutralize_Escape(Ada.Strings.Fixed.Trim(To_String(Command), Ada.Strings.Both)), " ");
                    Process_Command(System, Args_List, Menu_Active);
                    New_Line;
                    Put_Line("Appuyer sur une touche pour continuer ...");
                    Get_Immediate(Trash_Character);
                else
                    Menu_Active := False;
                end if;
            else
                Put("$ [" & Pwd(System, Args_List) & "] > ");
                Args_List := Split_String(Neutralize_Escape(Ada.Strings.Fixed.Trim(Get_Line, Ada.Strings.Both)), " ");
                if Unbounded_String_List.Get_Length(Args_List) /= 0 then
                    Command := Unbounded_String_List.Get_Element(Args_List);
                    Process_Command(System, Args_List, Menu_Active);
                end if;
            end if;
            exception
                when EXCEPTION_ROOT_NOT_INITIALIZED => Put_Line(To_String(Command) & ": root is not initialized.");
                when EXCEPTION_CANNOT_CHANGE_ROOT => Put_Line(To_String(Command) & ": root cannot be change, move or delete.");
                when EXCEPTION_NOT_A_FOLDER => Put_Line(To_String(Command) & ": path is not a folder.");
                when EXCEPTION_NOT_AN_ORDINARY_FILE => Put_Line(To_String(Command) & ": path is not an ordinary file.");
                when EXCEPTION_FILE_NOT_FOUND => Put_Line(To_String(Command) & ": file was not found.");
                when EXCEPTION_FILE_ALREADY_EXIST => Put_Line(To_String(Command) & ": file already exist.");
                when EXCEPTION_NOT_A_NUMERIC_VALUE => Put_Line(To_String(Command) & ": value parameter is not a value.");
                when EXCEPTION_INVALID_NUMBER_OF_ARGS => Put_Line(To_String(Command) & ": invalid number of arguments.");
                when EXCEPTION_MEMORY_ERROR => Put_Line(To_String(Command) & ": file systems error about memory. The system may be full.");
                when EXCEPTION_PATH_IS_SUBPATH => Put_Line(To_String(Command) & ": first path is a subpath of second path.");
        end;
    end loop;
end Main;
