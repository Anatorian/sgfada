with File_Package; use File_Package;
with Memory_Package; use Memory_Package;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Double_Linked_List_Package;

package File_System_Package is
    type File_System is private;

    package Unbounded_String_List is new Double_Linked_List_Package(Element_Type => Unbounded_String, "=" => Ada.Strings.Unbounded."=");
    use Unbounded_String_List;

    ROOT_FILE_NAME: constant Unbounded_String := To_Unbounded_String("/");
    CURRENT_FILE_NAME: constant Unbounded_String := To_Unbounded_String(".");
    PARENT_FILE_NAME: constant Unbounded_String := To_Unbounded_String("..");

    FIRST_INDEX_HDD: constant Natural := 1;
    SIZE_INODE: constant Natural := 10;
    SIZE_HDD: constant Natural := 1000000000;

    EXCEPTION_ROOT_NOT_INITIALIZED: Exception;
    EXCEPTION_CANNOT_CHANGE_ROOT: Exception;
    EXCEPTION_NOT_A_FOLDER: Exception;
    EXCEPTION_NOT_AN_ORDINARY_FILE: Exception;
    EXCEPTION_FILE_NOT_FOUND: Exception;
    EXCEPTION_FILE_ALREADY_EXIST: Exception;
    EXCEPTION_NOT_A_NUMERIC_VALUE: Exception;
    EXCEPTION_PATH_IS_SUBPATH: Exception;
    EXCEPTION_MEMORY_ERROR: Exception;
    EXCEPTION_INVALID_NUMBER_OF_ARGS: Exception;

    -- Name: Create
    -- Semantics: Create a new File_System.
    -- Parameters: ~
    -- Return type: File_System
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Create return File_System;

    -- Name: Pwd
    -- Semantics: Print Working Directory
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Return type: String
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS
    -- Preconditions: ~
    -- Postconditions: ~
    function Pwd(File_System_Arg: in File_System; Args_List: in out Double_Linked_List) return String;

    -- Name: Touch
    -- Semantics: Change the modification date time of a file or create it if it does not exist and if it's possible.
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_MEMORY_ERROR
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Touch(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Vim
    -- Semantics: Change a file's physical size.
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_MEMORY_ERROR, EXCEPTION_NOT_A_NUMERIC_VALUE
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Vim(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Mkdir
    -- Semantics: Create a new folder if possible.
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_ALREADY_EXIST, EXCEPTION_MEMORY_ERROR
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Mkdir(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Ls
    -- Semantics: List files
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Return type: String
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS
    -- Preconditions: ~
    -- Postconditions: ~
    function Ls(File_System_Arg: in File_System; Args_List: in out Double_Linked_List) return String;

    -- Name: Cd
    -- Semantics: Change working directory
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_NOT_A_FOLDER
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Cd(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Rm
    -- Semantics: Remove a file/directory
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_NOT_AN_ORDINARY_FILE, EXCEPTION_CANNOT_CHANGE_ROOT, EXCEPTION_MEMORY_ERROR
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Rm(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Mv
    -- Semantics: Move a file/directory from a source to a dest
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_FILE_ALREADY_EXIST, EXCEPTION_CANNOT_CHANGE_ROOT, EXCEPTION_PATH_IS_SUBPATH, EXCEPTION_MEMORY_ERROR
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Mv(File_System_Arg: in File_System; Args_List: in out Double_Linked_List);

    -- Name: Cp
    -- Semantics: Copy a file/directory from a source to a dest
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_NOT_AN_ORDINARY_FILE, EXCEPTION_FILE_ALREADY_EXIST, EXCEPTION_MEMORY_ERROR
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Cp(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Tar
    -- Semantics: Archive file/directory from a source to a dest
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS, EXCEPTION_FILE_NOT_FOUND, EXCEPTION_FILE_ALREADY_EXIST, EXCEPTION_MEMORY_ERROR
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Tar(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Defragment
    -- Semantics: Defragment the memory
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      Args_List: List of argument to send to the command.
    -- Exceptions: EXCEPTION_INVALID_NUMBER_OF_ARGS
    -- Preconditions: ~
    -- Postconditions: ~
    procedure Defragment(File_System_Arg: in out File_System; Args_List: in out Double_Linked_List);

    -- Name: Split_String
    -- Semantics: Split a string into a Double_Linked_List (use for Args_List) by a separator with an escape pattern string.
    -- Parameters:
    --      String_To_Split: String to split.
    --      Separator: Separator to use for string splitting.
    --      Escape: Escape pattern string to use for escape a character (by default "\")
    -- Return type: Double_Linked_List
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Split_String(String_To_Split: in String; Separator: in String; Escape: in String := "\") return Double_Linked_List;

    -- Name: Neutralize_Escape
    -- Semantics: Return a copy of a string with all escape patterns removed. (used on each element after Split_String).
    -- Parameters:
    --      String_To_Clean: String to clean/neutralize.
    --      Escape: Escape pattern string to use for escape a character (by default "\")
    -- Return type: String
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Neutralize_Escape(String_To_Clean: in String; Escape: in String := "\") return String;
private
    type File_System is record
        Root_File: Ptr_File;
        Current_File: Ptr_File;
        Allocator: Memory_Allocator;
    end record;

    -- Name: Get_Path_List
    -- Semantics: Use String_To_Split to split a path string such as "/home/raphael" or "../bin/" (used on all system command for path management)
    -- Parameters:
    --      String_To_Split: String to split into path list.
    -- Return type: Double_Linked_List
    -- Exceptions: ~
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Path_List(String_To_Split: in String) return Double_Linked_List;

    -- Name: Get_Absolute_Path
    -- Semantics: Return absolute path of a file
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      File: File in order to get absolute path.
    -- Return type: String
    -- Exceptions: EXCEPTION_FILE_NOT_FOUND
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Absolute_Path(File_System_Arg: in File_System; File: Ptr_File) return String;

    -- Name: Get_Total_Physical_Size
    -- Semantics: Return total physical size of a file
    -- Parameters:
    --      File: File in order to get its total size (all children size)
    -- Return type: Natural
    -- Exceptions: EXCEPTION_FILE_NOT_FOUND
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Total_Physical_Size(File: in Ptr_File) return Natural;

    -- Name: Get_File
    -- Semantics: Return a file/folder's child with name sent as parameter (used for name management such as "." ".." "/" and others).
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      File_Iterator: File to use for research child.
    --      Name: Child's name.
    -- Return type: Ptr_File
    -- Exceptions: EXCEPTION_FILE_NOT_FOUND,
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_File(File_System_Arg: in File_System; File_Iterator: in Ptr_File; Name: in Unbounded_String) return Ptr_File;

    -- Name: Get_Folder
    -- Semantics: Return a folder's child with name sent as parameter (used for name management such as ".", "..", "/" and others). If it's not a folder, raise EXCEPTION_NOT_A_FOLDER.
    -- Parameters:
    --      File_System_Arg: File_System to use for the command.
    --      File_Iterator: File to use for research child.
    --      Name: Child's name.
    -- Return type: Ptr_File
    -- Exceptions: EXCEPTION_FILE_NOT_FOUND, EXCEPTION_NOT_A_FOLDER
    -- Preconditions: ~
    -- Postconditions: ~
    function Get_Folder(File_System_Arg: in File_System; File_Iterator: in Ptr_File; Name: in Unbounded_String) return Ptr_File;

    -- Name: Valid_Name
    -- Semantics: Check if a name is valid (a file/folder cannot be created with the name ".", ".." and "/")
    -- Parameters:
    --      Name: File/folder's name.
    -- Return type: Unbounded_String
    -- Exceptions: EXCEPTION_FILE_ALREADY_EXIST
    -- Preconditions: ~
    -- Postconditions: ~
    function Valid_Name(Name: in Unbounded_String) return Unbounded_String;
end File_System_Package;
