with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with File_System_Package; use File_System_Package;

procedure Test_File_System is
    File_System_Arg: File_System;
    Args_List: Unbounded_String_List.Double_Linked_List := Unbounded_String_List.Create;

    EXCEPTION_NOT_OK: exception;
begin
    begin
        File_System_Arg := Create;
        Put_Line("Create OK");
    exception
        when others => Put_Line("Create NOK");
    end;

    begin
        if Pwd(File_System_Arg, Args_List) = "" then
            null;
        end if;
        Put_Line("Pwd OK");
    exception
        when others => Put_Line("Pwd NOK");
    end;

    begin
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
        Touch(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
        Touch(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
        Touch(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Touch(File_System_Arg, Args_List);
        Put_Line("Touch OK");
    exception
        when others => Put_Line("Touch NOK");
    end;

    begin
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
            Mkdir(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
            Mkdir(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
            Mkdir(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
        Mkdir(File_System_Arg, Args_List);
        Put_Line("Mkdir OK");
    exception
        when others => Put_Line("Mkdir NOK");
    end;

    begin
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("500"));
            Vim(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_NOT_AN_ORDINARY_FILE => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("500"));
            Vim(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_NOT_AN_ORDINARY_FILE => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("500"));
            Vim(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_NOT_AN_ORDINARY_FILE => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("500"));
            Vim(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_NOT_AN_ORDINARY_FILE => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("something"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("500"));
            Vim(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NOT_FOUND => NULL;
        end;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("500"));
        Vim(File_System_Arg, Args_List);
        Put_Line("Vim OK");
    exception
        when others => Put_Line("Vim NOK");
    end;

    begin
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
        if Ls(File_System_Arg, Args_List) = "" then
            NULL;
        end if;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
        if Ls(File_System_Arg, Args_List) = "" then
            NULL;
        end if;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
        if Ls(File_System_Arg, Args_List) = "" then
            NULL;
        end if;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
        if Ls(File_System_Arg, Args_List) = "" then
            NULL;
        end if;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        if Ls(File_System_Arg, Args_List) = "" then
            NULL;
        end if;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        if Ls(File_System_Arg, Args_List) = "" then
            NULL;
        end if;
        Put_Line("Ls OK");
    exception
        when others => Put_Line("Ls NOK");
    end;

    begin
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
        Cd(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
        Cd(File_System_Arg, Args_List);
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Cd(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_NOT_A_FOLDER => NULL;
        end;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
        Cd(File_System_Arg, Args_List);
        Put_Line("Cd OK");
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
        Cd(File_System_Arg, Args_List);
    exception
        when others => Put_Line("Cd NOK");
    end;

    begin
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("toto"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_PATH_IS_SUBPATH => NULL;
                when EXCEPTION_CANNOT_CHANGE_ROOT => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("toto"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_PATH_IS_SUBPATH => NULL;
                when EXCEPTION_CANNOT_CHANGE_ROOT => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("toto"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_PATH_IS_SUBPATH => NULL;
                when EXCEPTION_CANNOT_CHANGE_ROOT => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("ggreger"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("fzfzef"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_NOT_FOUND => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("fzfzef/fzefze"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_NOT_FOUND => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
            Mv(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
            exception
                when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("tata"));
        Mv(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("tata"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Mv(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory/file"));
        Mv(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory/file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Mv(File_System_Arg, Args_List);
        Put_Line("Mv OK");
    exception
        when others => Put_Line("Mv NOK");
    end;

    begin
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("rfezf"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("toto"));
            Cp(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NOT_FOUND => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Cp(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
            Cp(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
            Cp(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
            Cp(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_ALREADY_EXIST => NULL;
        end;
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("tata"));
            Cp(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_NOT_AN_ORDINARY_FILE => NULL;
        end;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("-r"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file1"));
        Cp(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("-r"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory1"));
        Cp(File_System_Arg, Args_List);
        Put_Line("Cp OK");
    exception
        when others => Put_Line("Cp NOK");
    end;

    begin
        begin
            Unbounded_String_List.Clear(Args_List);
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("rfezf"));
            Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("toto"));
            Tar(File_System_Arg, Args_List);
            raise EXCEPTION_NOT_OK;
        exception
            when EXCEPTION_FILE_NOT_FOUND => NULL;
        end;
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("."));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file."));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String(".."));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file.."));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("/"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("fileSlash"));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("file"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("fileArchive"));
        Tar(File_System_Arg, Args_List);
        Unbounded_String_List.Clear(Args_List);
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directory"));
        Unbounded_String_List.Append_Last_Element(Args_List, To_Unbounded_String("directoryArchive"));
        Tar(File_System_Arg, Args_List);
        Put_Line("Tar OK");
    exception
        when others => Put_Line("Tar NOK");
    end;

    begin
        Unbounded_String_List.Clear(Args_List);
        Defragment(File_System_Arg, Args_List);
        Put_Line("Defragment OK");
    exception
        when others => Put_Line("Defragment NOK");
    end;

    begin
        if Unbounded_String_List.Get_Length(Split_String("", " ")) /= 0 then
            raise EXCEPTION_NOT_OK;
        end if;
        if Unbounded_String_List.Get_Length(Split_String("     ", " ")) /= 0 then
            raise EXCEPTION_NOT_OK;
        end if;
        if Unbounded_String_List.Get_Length(Split_String("mkdir toto", " ")) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        if Unbounded_String_List.Get_Length(Split_String("mkdir              toto", " ")) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        if Unbounded_String_List.Get_Length(Split_String("mkdir toto\ toto", " ")) /= 2 then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Split_String OK");
    exception
        when others => Put_Line("Split_String NOK");
    end;

    begin
        if Neutralize_Escape("\toto") /= "toto" then
            raise EXCEPTION_NOT_OK;
        end if;
        if Neutralize_Escape("\ toto") /= " toto" then
            raise EXCEPTION_NOT_OK;
        end if;
        if Neutralize_Escape("") /= "" then
            raise EXCEPTION_NOT_OK;
        end if;
        Put_Line("Neutralize_Escape OK");
    exception
        when others => Put_Line("Neutralize_Escape NOK");
    end;
end Test_File_System;
