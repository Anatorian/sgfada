# **User manual**

In order to launch the project, you need to compile the [main.adb](../src/main.adb) file and run it. An interpreter should launch and it will be possible to execute commands.

## **Table of contents**

* [Pwd](#Pwd)
* [Touch](#Touch)
* [Mkdir](#Mkdir)
* [Cd](#Cd)
* [Ls](#Ls)
* [Vim](#Vim)
* [Rm](#Rm)
* [Mv](#Mv)
* [Cp](#Cp)
* [Tar](#Tar)
* [Defragment](#Defragment)
* [Menu](#Menu)

### **Pwd**
Print the full filename of the current working directory.

<pre>
$ [/] > pwd
</pre>

### **Touch**
Update the modification times of file to the current time.

<pre>
$ [/] > touch [<span style="color:#9B59B6">FILE</span>]
</pre>

### **Mkdir**
Make directory(ies), if they do not already exist.

<pre>
$ [/] > mkdir [<span style="color:#9B59B6">PATH</span>]
</pre>

### **Cd**
The cd utility shall change the working directory of the current shell execution.

<pre>
$ [/] > cd [<span style="color:#9B59B6">PATH</span>]
</pre>

### **Ls**
List information about the FILEs (the current directory by default).

<pre>
$ [/] > ls [<span style="color:#9B59B6">OPTION</span>] [<span style="color:#9B59B6">PATH</span>]
</pre>

*   -r : Apply ls recursively on each FILEs

### **Vim**
Vi IMproved, a programmer's text editor (change physical size only).

<pre>
$ [/] > vim [<span style="color:#9B59B6">FILE</span>] [<span style="color:#9B59B6">SIZE</span>]
</pre>

### **Rm**
Rm removes specified file. By default, it does not remove directories

<pre>
$ [/] > rm [<span style="color:#9B59B6">OPTION</span>] [<span style="color:#9B59B6">PATH</span>]
</pre>

*   -r : Apply rm recursively on directory

### **Mv**
Rename SOURCE to DEST, or move SOURCE to DIRECTORY.

<pre>
$ [/] > mv [<span style="color:#9B59B6">SOURCE</span>] [<span style="color:#9B59B6">DEST</span>]
</pre>

### **Cp**
Copy SOURCE to DEST, or multiple SOURCE to DIRECTORY.

<pre>
$ [/] > cp [<span style="color:#9B59B6">OPTION</span>] [<span style="color:#9B59B6">SOURCE</span>] [<span style="color:#9B59B6">DEST</span>]
</pre>

*   -r : Apply cp recursively on directory

### **Tar**
Archiving utility.

<pre>
$ [/] > tar [<span style="color:#9B59B6">SOURCE</span>] [<span style="color:#9B59B6">DEST</span>]
</pre>

### **Defragment**
Defragment physical storage.

<pre>
$ [/] > defragment
</pre>

### **Menu**
Display a menu to choose a specific command.

<pre>
$ [/] > menu
</pre>
