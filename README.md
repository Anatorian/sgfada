
<div align="center">

# **Projet de méthodologie de la programmation**

</div>

<div align="justify">

## **Résumé du rapport**
Ce rapport à pour but de regrouper les différents aspects de ce projet consistant en la création d'un programme de gestion de fichiers (que nous détaillons dans la partie [introduction au projet](#introduction-au-projet)). Nous commencerons par une introduction du projet afin de placer le contexte puis nous poursuivrons sur une présentation de l'architecture et de la conception derrière le code. Enfin nous finirons sur les tests des modules et sur un bilan général du projet aussi bien d'un point de vue technique que personnel.

### **Sommaire**

* [Manuel utilisateur (en anglais)](docs/USER_MANUAL.md)
* [Introduction](#introduction-au-projet)
* [Déroulement du projet](#déroulement-du-projet)
* [Concernant les raffinages](#concernant-les-raffinages)
* [Architecture globale du projet](#architecture-globale-du-projet)
* [Présentation des choix de conception](#présentation-des-choix-de-conception)
* [Les tests des modules](#les-tests)
* [Difficultés rencontrées](#difficultés-rencontrées)
* [Bilan](#bilan)

## **Introduction au projet**
Ce projet à pour but de créer un programme de gestion de fichiers. De façon générale, un système de fichiers est une façon de stocker les informations et de les organiser dans des fichiers sur des mémoires secondaires (disque dur, disque SSD, clé USB ...). Cette gestion des fichiers permet de traiter et de conserver des quantités importantes de données (source [wikipedia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_fichiers)).\
Afin de réaliser ce programme, je me suis basé sur un système d'exploitation de type UNIX (avec une racine "/" et certaines structures de données déjà présentes comme les inodes) et sur les commandes habituelles de ces systèmes d'exploitation telle que cd, ls ou encore mkdir (chacune décrites dans le [manuel utilisateur](docs/USER_MANUAL.md)).

Ce rapport contiendra les différentes facettes de ce projet, en passant par l'architecture du projet, les choix de conception, les difficultés rencontrées et bien plus encore.

## **Déroulement du projet**
Le déroulement du projet s'est passé de la manière suivante :
1. Conception architecture et divers schéma/diagrammes.
2. Écriture des TAD et des raffinages (non-détaillé en pseudocode).
3. Implémentation des structures de données, des fonctions et procédures jusqu'à ce qu'un problème survienne.
4. Écriture des tests et retour sur raffinage (en détaillant cette fois-là).
5. Retour sur implémentation pour finir.

Ce mode de fonctionnement dans un projet me convient bien et me permet d'avancer à une vitesse décente et de voir concrètement que je fais.

## **Concernant les raffinages**
Concernant les raffinages dans ce projet, ne voulant pas mettre deux fois le même code (en pseudocode et en ADA) j'ai pris la décision de ne pas insérer tous les raffinages. Seuls les raffinages réellement importants seront présents (ceux qui permettent de comprendre le code parfois un peu complexe). De ce fait, étant donné que j'ai essayé de faire le code le plus propre possible (nom de variables explicites, nom de procédure/fonctions explicites etc), le nombre de commentaire a été réduit pour n'avoir que les informations importantes. Par exemple les raffinages des getter et setter ont été supprimé, ceux des fonctions/procédures basiques aussi.\
Cependant tous les contrats sont disponible dans les fichiers de spécifications permettant de mieux comprendre l'architecture globale du projet.

## **Architecture globale du projet**
*Dans cette partie nous considérerons qu'un fichier est une structure de données générique par rapport à son type et donc pouvant être de type "fichier ordinaire" ou "dossier". Un fichier et un dossier sont donc similaire dans leur structure si ce n'est qu'un de leur attribut (le type) change.*

À présent que le contexte est posé, attardons nous sur l'architecture globale du projet.\
Le projet suit plusieurs concepts que j'ai voulu mettre en oeuvre:
* L'encapsulation de la conception objet (concept que j'apprécie beaucoup)
* La composition de structure que j'utilise dans mes projets personnels et qui me convient parfaitement.

Le projet est constitué de 6 modules différents :
  * [les blocs de mémoires](#bloc-mémoire),
  * [les inodes](#inode) (métadonnées d'un fichier),
  * [les fichiers](#fichier),
  * [la gestion de mémoire](#gestion-de-la-mémoire),
  * [le gestionnaire de fichier](#gestionnaire-de-fichier),
  * [les listes doublement chaînée](#liste-doublement-chaînée) (générique)

### **Bloc mémoire**
Commençons par le bas de l'échelle à savoir un bloc mémoire. Ce bloc de mémoire représente un espace dans la capacité totale allouable (une capacité allouable complètement imaginée), de ce fait, il possède seulement un index de début et une taille. Il sera utilisé par la suite dans le reste du projet afin de faire la gestion de la mémoire.

### **Inode**
Les inodes sont les métadonnées d'un fichier, ils sont constitués d'un type de fichier (fichier ordinaire ou dossier dans le cas de ce projet), d'un bloc mémoire, d'une date de création et d'une date de modification. Étant donné que ce sont les métadonnées d'un fichier, chaque fichier possède exactement un inode.

### **Fichier**
À présent que nous avons les structures de bases, nous pouvons réaliser la structure la plus importante de ce projet : les fichiers. Un fichier est une structure de données composée d'un nom, un inode, un parent et une liste d'enfants (même si un fichier ordinaire ne peut pas avoir d'enfants).\
La structure de fichier est au final un arbre avec N nœud non-défini à la compilation, chaque fichier possède une liste d'enfant et un parent (sauf le dossier racine) d'où le schéma suivant:

<div align="center"><img src="docs/FileTree.png"></div>

Cette partie étant la plus importante, nous reviendrons dessus dans la partie de [conception](#présentation-des-choix-de-conception)

### **Gestion de la mémoire**
La gestion de la mémoire est possible grâce à ce module mettant à disposition une structure de données : l'allocateur de mémoire ainsi que les algorithmes permettant d'allouer, de désallouer et de réarranger la mémoire.\
L'allocateur de mémoire est composé d'un index de début (de la mémoire secondaire), la taille totale (de la mémoire secondaire) ainsi que deux listes représentant les blocs mémoire alloués et ceux non alloués.

### **Gestionnaire de fichier**
Au final, nous avons le module principal, celui qui est ensuite utilisé pour créer l'application finale (le main), le module du gestionnaire de fichier.\
Le module est composé d'une structure de données contenant le dossier racine ainsi que le dossier courant et l'allocateur de mémoire. Ce module possède aussi toutes les opérations nécessaire afin de faire évoluer le système de fichier (depuis cd en passant par mkdir et par rm, toutes les commandes se trouvent dans le [manuel utilisateur](docs/USER_MANUAL.md))\
En résumé ce module est juste l'interface afin de pouvoir gérer la mémoire et les fichiers du système.

### **Liste doublement chaînée**
Durant la conception de ce programme, plusieurs questions nous sommes venues et surtout une: quelle structure de données est la plus adaptée afin de stocker dynamiquement des éléments quelconques ? Pour répondre à cette question, je me suis penché sur les différentes structures de données vu jusqu'à présent et il me semblait bon d'utiliser les listes doublement chaînée pour faire ce travail (dans le contexte de ce projet). Nous y reviendrons un peu plus tard dans ce rapport dans la partie [des choix de conception](#présentation-des-choix-de-conception)

### **Diagramme de dépendances**
On peut représenter l'interaction de ces modules de part ce diagramme de dépendances:

<div align="center"><img src="docs/DiagramDependency.png"></div>

## **Présentation des choix de conception**
Nous avons vu une description globale du projet (contexte et architecture). Cette partie permettra d'appronfondir les choix de conceptions et d'implémentation fait durant toute la durée du projet. 

### **Structure de données de type conteneur**
Définissons le problème, il faut une structure de données capable de s'adapter dynamiquement (après la création) afin qu'un dossier ne soit pas bloqué avec un nombre N d'enfants défini à la compilation et donc immuable.

De ce fait il faut définir la structure de données appropriée afin d'avoir le conteneur le plus pratique pour ce projet (en particulier pour stocker les enfants d'un dossier), pour cela, listons les conteneurs vus en cours jusqu'à présent:

<div align="center">

| **Structure de donnée** |      **Dynamique**      |
| :---------------------: | :---------------------: |
|     Enregistrement      |           Non           |
|         Tableau         | À la création seulement |
|          Liste          |           Oui           |
|          Pile           |           Oui           |
|    Arbre (équilibré)    |       Oui et non*       |

**Les arbres sont des structures de données dynamiques, cependant, il est impossible pour un noeud d'avoir un nombre de sous-arbres variable après la compilation, ce qui nous embête dans notre cas.*

</div>

Déjà nous pouvons constater que certaines structure de données ne sont pas utilisables (dans ce contexte là), les enregistrements, les tableaux et les arbres, car elles ne sont pas dynamiques (comme souhaité).\
Penchons nous sur les autre structures de données:

<div align="center">

| **Structure de donnée** | **Recherche d'un élément** |
| :---------------------: | :------------------------: |
|          Liste          |            O(n)            |
|          Pile           | Pas de recherche d'élément |

</div>

On peut voir qu'au final les piles ne sont pas utilisables dans le cadre de ce projet. Il reste donc seulement les listes. Celle-ci nous permettront d'avoir une structure en arbre:
* Un "noeud" (fichier) possède des enfants (des sous-arbres),
* Un fichier connaît son parent, on peut donc remonter à lui (jusqu'à la racine si on le souhaite).

Nous réutilisons ces listes pour l'allocateur de mémoire ainsi que pour les listes d'arguments pour les opérations sur le système de fichier (cd, mkdir etc) (comme vu dans le [diagramme de dépendances](#diagramme-de-dépendances)).

### **Utilisation des pointeurs (fichier, inode, mémoire)**
Plus tôt dans ce rapport, il était indiqué que un inode possédait un bloc mémoire, qu'un fichier possédait un inode etc. Cependant il était impossible de raisonner de cette manière et nous étions obligé de passé exclusivement par des pointeurs par rapport à ces 3 structures de données.

* Les blocs mémoire: il sont délivrés par l'allocateur de mémoire, or comme défini plus tôt, l'allocateur de mémoire possède une liste de blocs mémoire alloué (pour garder le contrôle dessus). Donc étant donné que l'inode ainsi que lui possèdent le même bloc mémoire (à deux endroits différents), il a fallut utiliser un pointeur. De ce fait l'inode et l'allocateur possède exactement le même bloc mémoire.
* Les inodes: il était important de penser à l'évolution du programme et, m'étant basé sur les systémes UNIX, j'ai laissé la possibilité à une ouverture vers de nouveaux types de fichier, comme les liens symbolique. Les liens symboliques sont des raccourcis vers un fichier. De ce fait il partage exactement le même inode que le fichier original et donc il était important d'utiliser des pointeurs pour que les deux fichiers (original et raccourcis) puissent partager le même inode.
* Les fichiers: la structure même d'un fichier oblige à utiliser des pointeurs. Un fichier A possède des enfants et les enfants possède un parent (le parent étant le fichier A). Dans ce cas il y a une obligation de passer par des pointeurs.

### **Le module de gestion de fichier**
Le module de gestion de fichier est probablement le plus complexe. En effet l'architecture est faite de telle sorte que la gestion des chemins et la gestion des arguments d'une commande sont les mêmes pour toute commande :
1.  Avant tout, on récupère la commande entrée (sachant que le menu crée une chaîne de caractère qui ressemble à une commande en tout point),
2.  On découpe la chaîne de caractères (en fonction des espaces) afin d'en octroyer la commande et ses arguments,
3.  On supprime la nom de la commande de la chaîne et on envoie le reste à la fonction/procédure souhaitée.

Par la suite la fonction/procédure effectue des opérations de la manière suivantes :
1.  On récupère chaque argument un par un et on le stocke,
2.  Si l'argument est une option on met un booléen à vrai ou à faux en fonction de ce qui a été reçu,
3.  Sinon si l'argument est un chemin, on découpe la chaîne reçu par les "/" grâce à une fonction (Get_Path_List),
4.  Puis une fois tous les arguments traités, on effectue les opérations nécessaires pour la fonction/procédure en question.

## **Les test des modules**
Chaque module a été testé un par un, tous les tests établis sont fonctionnels (normalement), ils permettent de voir que toutes les exceptions/cas ont été pris en compte, sachant que la plupart du projet et fait en programmation défensive (que je trouve plus logique dans ce contexte, je juge que le SGF pourrait être utilisé ailleurs pour par exemple une application graphique de gestion de fichiers comme Thunar ou Nautilus qu'une communauté X voudrait faire).\
Tous les tests commencent par le même intitulé de fichier à savoir test_*.adb

## **Difficultés rencontrées**
Si on regarde les difficultés rencontrées, celles-ci ne se trouvent pas vraiment du côté du code mais plutôt de la gestion du temps.

### **Les chaînes de caractère**
Le premier problème que j'ai rencontré fut de savoir comment avoir une chaîne de caractère qui pouvait varier de part sa longueur. Cependant le problème a été vite résolu suite à quelques recherches, il suffit d'utiliser le module *Ada.Strings.Unbounded*.

### **Les dépendances circulaires**
Un des problèmes fut la dépendance circulaire. Mon instanciation du package de liste (pour les enfants d'un fichier) était à la base en privé et je ne voyais pas l'intérêt de la mettre en publique. Cependant lorsque j'ai codé la fonction pour retourner la liste des enfants, il me fallait un moyen de la manipuler. S'en est suivi un long processus de compréhension.\
L'idée première fut de mettre l'instanciation de la liste dans un nouveau module, cependant si je faisais ça, la liste avait besoin du module de fichier et inversement ce qui causa une dépendance circulaire.\
Finalement j'ai décidé de mettre l'instanciation de ma liste en publique dans le module de fichier, cela reste cohérent donc je pense que ce n'est pas une mauvaise chose, j'ai par ensuite réarrangé le code afin d'avoir quelque chose de convaincant.

### **La gestion du temps**
La gestion du temps fut un sérieux problème, j'avais tendance à prendre trop de temps pour aider les autres ou parler de conception avec mes camarades ce qui m'a fait perdre énormément de temps étant donné que ma conception était déjà élaboré.

### **Ma volonté d'en faire toujours plus**
Ce problème est un problème qui est similaire à la gestion du temps. Je veux toujours en faire plus jusqu'à avoir quelque chose qui me plaise vraiment. Cependant il n'est pas toujours évident de conclure un projet (comme on le souhaite) dans le temps imparti.

## **Bilan**
### **Bilan technique**
Techniquement parlant, ce projet m'a apporté deux choses :
* Mise en valeur de l'utilité des raffinages et des tests et en général de la méthodologie de la programmation,
* Apprentissage d'un nouveau langage l'ADA

Étant de base une personne qui avait fait un certain nombre de projet en IUT, j'ai pris l'habitude de cela. De même étant habitué à la conception (ma phase favorite), je n'ai pas forcément appris énormément de part ce projet.\
Cependant, avant ce projet, je ne connaissais pas grand choses quant à la méthodologie de programmation. J'attendais ce projet pour me montrer que la méthodologie enseignée était quelque chose d'important afin de ne pas se perdre dans un tel projet, et effectivement, cette méthodologie fut nécessaire.

### **Bilan personnel**
Comme dit juste au dessus, j'ai fait beaucoup de projet en IUT mais aussi en dehors du cadre scolaire, de ce fait d'un point de vue personnel, cela ne m'a pas appris énormément.\
Cependant je m'aperçois, sur un petit projet comme celui-ci, que j'ai du mal à gérer mon temps. Je passe beaucoup de temps à aider les autres personnes où à parler de conception ce qui me fait défaut. De même je pense que je travail beaucoup trop quand ce genre de projet est demandé.

### **Temps passé**

Voici un tableau récapitulatif du temps que j'ai passé aux différentes étapes de ce projet:

<div align="center">

|       **Étape**        | **Temps** |
| :--------------------: | :-------: |
|       Conception       |   5-6h    |
|    Raffinage/Tests     |    15h    |
|          Code          |    27h    |
|    Aider les autres    |    21h    |
|       Aider Pôl        |    9h     |
| Discuter de conception |    12h    |
|        Rapport         |    13h    |

</div>

</div>
